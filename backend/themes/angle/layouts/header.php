<?php
//use Yii;
use yii\helpers\Url;
?>
 <!-- top navbar-->
<header class="topnavbar-wrapper">
    <!-- START Top Navbar-->
    <nav role="navigation" class="navbar topnavbar">
       <!-- START navbar header-->
        <div class="navbar-header">
            <a href="<?=Url::to(['/esakhi/dashboard'])?>" class="navbar-brand">
                <div class="brand-logo">
                    <?php echo '<img class="img-responsive" 
                    src="'.Yii::$app->request->baseUrl.'/angle/img/logo.png"
                    alt="angle">';?>
                      
                </div>
                <!-- <div class="brand-logo-collapsed">
                    <?php echo '<img class="img-responsive huddle-single-logo" 
                    src="'.Yii::$app->request->baseUrl.'/angle/img/logo.png"
                    alt="angle" >';?>
                </div> -->
            </a>
        </div>
       <!-- END navbar header-->
       <!-- START Nav wrapper-->
       <div class="nav-wrapper">
          <!-- START Left navbar-->
            <ul class="nav navbar-nav">
                <li>
                <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
                    <!-- <a href="#" data-trigger-resize="" 
                       data-toggle-state="aside-collapsed">
                       <em class="fa fa-navicon"></em>
                    </a> -->
                    <!-- Button to show/hide the sidebar on mobile. Visible on mobile only.-->
                    <a href="#" data-toggle-state="aside-toggled" 
                       data-no-persist="true" class="visible-xs sidebar-toggle">
                       <em class="fa fa-navicon"></em>
                    </a>
                </li>

            </ul>
            <?php
            if (!\Yii::$app->user->isGuest) {
                ?>
                <ul class="nav navbar-nav navbar-right">
	          <!-- Search icon-->
                  <li >
                     <a href="<?php echo Url::to(['/user-management/auth/logout']);?>" data-toggle-fullscreen="">
                        <?=Yii::t('app', 'Logout ( {username} )', ['username' => Yii::$app->user->identity->username])?>
                     </a>
                  </li>
            </ul> 
                <?php
            }?>
                 
          <!-- END Left navbar-->
        </div>
       <!-- END Nav wrapper-->
    </nav>
    <!-- END Top Navbar-->
</header>
