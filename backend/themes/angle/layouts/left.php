<?php 

// use Yii;
use common\helpers\GeneralHelper;
use webvimark\modules\UserManagement\components\GhostMenu;
?>

<!-- sidebar-->
    <aside class="aside">
         <!-- START Sidebar (left)-->
        <div class="aside-inner">
            <nav data-sidebar-anyclick-close="" class="sidebar">
               <!-- START sidebar nav-->
               
            <?php
                      
                $menuItems = GeneralHelper::getBackendMenuItems();
            
                ?>
                <?= GhostMenu::widget(
                    [
                        'encodeLabels' => false,
                        'activateParents' => true,
                        'items' => $menuItems,
                        'submenuTemplate' => "\n
                        {items}\n</ul>\n",
                        'options' => ['class' => 'nav'],
                    ]
                ) ?>
               
               <!-- END sidebar nav-->
            </nav>
        </div>
         <!-- END Sidebar (left)-->
    </aside>

      
