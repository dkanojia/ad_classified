<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
  </head>
  <body class="">
    <?php $this->beginBody() ?>
      <div class="wrapper">
    <!-- BEGIN HEADER -->
       <?php echo $this->render('header.php') ?>
      <!-- END HEADER -->
      <!-- BEGIN CONTENT -->
      
        <!-- BEGIN SIDEBAR -->
         <?php echo $this->render('left.php') ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE CONTAINER-->
        <section>
             <?php if(!(isset($this->params['show_breadcrumbs']) && $this->params['show_title'] == false)): ?>
             <!--  <div class="page-title">
                  <h3 class="page-title-header"><?php echo Html::encode($this->title) ?></h3>
              </div> -->
            <?php endif; ?>
            <?php if(!(isset($this->params['show_breadcrumbs']) && $this->params['show_breadcrumbs'] == false)): ?>
              <?php echo Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
              ]) ?>
            <?php endif; ?>
             <?= Alert::widget() ?>
          <div class="content-wrapper">
           
            <!-- BEGIN PlACE PAGE CONTENT HERE -->
            <?= $content ?>
            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
        </section>
        <!-- END PAGE CONTAINER -->
        <!-- BEGIN CHAT -->
          <?php //echo $this->render('chat.php') ?>
        <!-- END CHAT -->
     
      <!-- END CONTENT -->
      </div>
    <?php $this->endBody() ?>
    <div id="modal_popup"></div>
</body>
</html>
<?php $this->endPage() ?>
