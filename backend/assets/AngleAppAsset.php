<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AngleAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'angle/css/bootstrap.css',
        'angle/css/app.css',
        'angle/vendor/fontawesome/css/font-awesome.min.css',
        'angle/vendor/simple-line-icons/css/simple-line-icons.css',
        'angle/vendor/animate.css/animate.min.css',
        'angle/vendor/whirl/dist/whirl.css',
        'angle/vendor/datatables-colvis/css/dataTables.colVis.css',
        'angle/vendor/datatables/media/css/dataTables.bootstrap.css',
        'angle/vendor/dataTables.fontAwesome/index.css',
        'css/site.css',
    ];
    public $js = [
        //'angle/vendor/modernizr/modernizr.custom.js',
        //'angle/vendor/matchMedia/matchMedia.js',
        //'angle/vendor/jquery/dist/jquery.js',
        'angle/vendor/bootstrap/dist/js/bootstrap.js',
        'angle/vendor/jQuery-Storage-API/jquery.storageapi.js',
        //'angle/vendor/jquery.easing/js/jquery.easing.js',
        //'angle/vendor/animo.js/animo.js',
        'angle/vendor/slimScroll/jquery.slimscroll.min.js',
        //'angle/vendor/screenfull/dist/screenfull.js',
        //'angle/vendor/jquery-localize-i18n/dist/jquery.localize.js',
        //'angle/js/demo/demo-rtl.js',
        'angle/vendor/datatables/media/js/jquery.dataTables.min.js',
        //'angle/vendor/datatables-colvis/js/dataTables.colVis.js',
        //'angle/vendor/datatables/media/js/dataTables.bootstrap.js',
        'angle/vendor/datatables-buttons/js/dataTables.buttons.js',
        //'angle/vendor/datatables-buttons/js/buttons.bootstrap.js',
        //'angle/vendor/datatables-buttons/js/buttons.colVis.js',
        //'angle/vendor/datatables-buttons/js/buttons.flash.js',
        //'angle/vendor/datatables-buttons/js/buttons.html5.js',
        //'angle/vendor/datatables-buttons/js/buttons.print.js',
        'angle/vendor/datatables-responsive/js/dataTables.responsive.js',
        'angle/vendor/datatables-responsive/js/responsive.bootstrap.js',
        'angle/vendor/Chart.js/dist/Chart.js',
        
        'angle/js/app.js',
        
    ];
    public $depends = [
    ];
}
