<?php
$params = array_merge(
    require __DIR__ . '/../../../common/config/params.php',
    require __DIR__ . '/../../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);
$aliases = require(__DIR__. '/alias.php');

return [
    'id' => 'app-api',    
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\v1\controllers',
    'bootstrap' => ['log'],
    'language' => 'en-US',
    'timeZone' => 'Asia/Kolkata',
    'aliases' => $aliases, 
    'modules' => [
        'user-management' => [
            'class' => 'webvimark\modules\UserManagement\UserManagementModule',
        ],
    ],   
    'components' => [
        'request' => [
            'parsers' => [
              'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'user' => [
            'class' => 'webvimark\modules\UserManagement\components\UserConfig',
            'identityClass' => 'backend\models\User'
        ],
        // 'user' => [
        //     'identityClass' => 'common\models\User',
        //     'enableAutoLogin' => true,
        //     'identityCookie' => ['name' => '_identity-api', 'httpOnly' => true],
        // ],

        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-api',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            // 'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        
    ],
    'params' => $params,
];
