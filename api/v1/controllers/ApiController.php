<?php

namespace api\v1\controllers;

use Yii;
use yii\rest\ActiveController;

class ApiController extends ActiveController
{
	public $modelClass = 'common\models\ApiLog';
	
	public $request;
    public $request_array;
    public $data;


    public function init(){
        parent::init();

        $post   = file_get_contents("php://input");
        $this->request  = $post;

        $post   = json_decode($post, true);
        if(empty($post)){
            $post = $_POST;
        }
	}
}