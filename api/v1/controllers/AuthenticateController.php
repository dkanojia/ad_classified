<?php
namespace api\v1\controllers;

use Yii;

use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;

use yii\web\Controller;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\console\Exception;

use common\models\WebsiteUsers;
/**
 * Auth controller

 */
class AuthenticateController extends ApiController
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'common\models\User';
    public function behaviors()
    {
        // $this->enableCsrfValidation = false;
        $behaviors = parent::behaviors();
        $behaviors['bootstrap'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                '' => ['post'],
            ],
        ];
        // For cross-domain AJAX request
        $behaviors['corsFilter']  = [
            'class' => \yii\filters\Cors::className(),
            'cors'  => [
                // Restrict access to
                'Access-Control-Allow-Origin' => ['*'],

                // restrict access to domains:
                'Origin' => ['http://backend.worldonrent.com', 'http://localhost/'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
                'Access-Control-Expose-Headers' => []
            ],
        ];

        return $behaviors;
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionLogin()
    {
        $post = file_get_contents("php://input");
        $post = json_decode($post, true);
        
        $response = [];

        try{   
            $website_users_response = WebsiteUsers::getUserList($post); 
            if($website_users_response['code'] != 200){
                return $website_users_response;
            }
            $response = ["success" => true, "code" => 200, "data" => $website_users_response['data']];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }

        return $response;
    }
    public function actionSignup()
    {
        $post = file_get_contents("php://input");
        $post = json_decode($post, true);
        
        $response = [];
        
        try{   
            $signup_response = WebsiteUsers::signUp($post); 
            if($signup_response['code'] != 200){
                return $signup_response;
            }
            $response = ["success" => true, "code" => $signup_response['code'],"message" => "Sign Up Successfully" ,"access_token" => $signup_response['access_token'],"data" => $signup_response['data']];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }

        return $response;
    }

    public function actionChangePassword()
    {
        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        $header_response = AdminUser::verifyApis($post['access_token']);
        if($header_response['code'] != 200){
            return $header_response;
        }
        $response = [];
        try{   
            $change_password_response = WebsiteUsers::changePassword($post); 
            if($change_password_response['code'] != 200){
                return $change_password_response;
            }
            $response = ["success" => true, "code" => 200,"message" => "Password Changed Successfully"];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionForgetPassword()
    {
        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        // $header_response = WebsiteUsers::verifyApis($post['access_token']);
        // if($header_response['code'] != 200){
        //     return $header_response;
        // }
        $response = [];
        try{   
            $forget_password_response = WebsiteUsers::forgetPassword($post); 
            if($forget_password_response['code'] != 200){
                return $forget_password_response;
            }
            $response = ["success" => true, "code" => 200,"message" => $forget_password_response['message']];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }
    
}
