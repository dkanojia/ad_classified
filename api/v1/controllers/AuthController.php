<?php
namespace api\v1\controllers;

use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use common\models\AdminUser;
use yii\rest\ActiveController;
use Yii;
use yii\console\Exception;
/**
 * Auth controller

 */
class AuthController extends ApiController
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'common\models\User';
    public function behaviors()
    {
        // $this->enableCsrfValidation = false;
        $behaviors = parent::behaviors();
        $behaviors['bootstrap'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                '' => ['post'],
            ],
        ];
        // For cross-domain AJAX request
        $behaviors['corsFilter']  = [
            'class' => \yii\filters\Cors::className(),
            'cors'  => [
                // Restrict access to
                'Access-Control-Allow-Origin' => ['*'],

                // restrict access to domains:
                'Origin' => ['http://backend.worldonrent.com', 'http://localhost/'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
                'Access-Control-Expose-Headers' => []
            ],
        ];

        return $behaviors;
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionAdminLogin()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        $email = $post['email'];
        $password = hash('gost',$post['password']);
        try{   
            $valid_auth = AdminUser::checkAuth($email,$password); 
            if($valid_auth){
                
                $response = ["success" => true, "code" => 200, "data" => ['message' => 'Successfully logged In.','access_token' => $valid_auth]];
            }else{
                $response = ["success" => false, "code" => 400, "data" => ['message' => 'INCORRECT_CREDENTIALS']];
            }
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionAdminLogout()
    {
        $response = [];
        $header_response = AdminUser::verifyHeaders(Yii::$app->request->headers);
        if($header_response['code'] != 200){
            return $header_response;
        }

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{    
            if(AdminUser::logout()){
                $response = ["success" => true, "code" => 200, "data" => ['message' => 'Logout Successfully']];
            }else{
                $response = ["success" => false, "code" => 400, "data" => ['message' => 'Please Entered Correcet Credentials']];
            }
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }

}
