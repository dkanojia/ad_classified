<?php
namespace api\v1\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\filters\ContentNegotiator;
use yii\web\Response;
/**
 * Site controller

 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        
        $behaviors['bootstrap'] = [
            'class'   => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['verbs'] = [
          'class'   => VerbFilter::className(),
          'actions' => [
            ],
        ];
        // For cross-domain AJAX request
        $behaviors['corsFilter']  = [
            'class' => \yii\filters\Cors::className(),
            'cors'  => [
                // Restrict access to
                'Access-Control-Allow-Origin' => ['*'],

                // restrict access to domains:
                'Origin' => ['http://backend.worldonrent.com', 'http://localhost/'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
                'Access-Control-Expose-Headers' => []
            ],
        ];
        return $behaviors;
  }

  

   
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return json_encode(array('V1 API is working!'));
    }
    
}
