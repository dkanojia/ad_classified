<?php
namespace api\v1\controllers;

use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;


use yii\rest\ActiveController;
use Yii;
use yii\console\Exception;

use common\models\WebsiteUsers;
use common\models\AdminUser;
use common\models\AdDetail;
use common\models\Tnc;
use common\models\AboutUs;
use common\models\WebsiteEnquiry;
use common\models\Categories;

use common\models\UploadForm;
use yii\web\UploadedFile;

/**
 * Auth controller

 */
class AdminController extends ApiController
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'common\models\User';
    public function behaviors()
    {
        // $this->enableCsrfValidation = false;
        $behaviors = parent::behaviors();
        $behaviors['bootstrap'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                '' => ['post'],
            ],
        ];
        // For cross-domain AJAX request
        $behaviors['corsFilter']  = [
            'class' => \yii\filters\Cors::className(),
            'cors'  => [
                // Restrict access to
                'Access-Control-Allow-Origin' => ['*'],

                // restrict access to domains:
                'Origin' => ['http://backend.worldonrent.com', 'http://localhost/'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
                'Access-Control-Expose-Headers' => []
            ],
        ];

        return $behaviors;
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionUserList()
    {
        $response = [];
        $header_response = AdminUser::verifyHeaders(Yii::$app->request->headers);
        if($header_response['code'] != 200){
            return $header_response;
        }

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            $website_users = WebsiteUsers::getList(); 
                $response = ["success" => true, "code" => 200, "data" => ['website_users_list' => $website_users]];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionAdList()
    {
        $response = [];
        $header_response = AdminUser::verifyHeaders(Yii::$app->request->headers);
        if($header_response['code'] != 200){
            return $header_response;
        }

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            $ad_details = AdDetail::getList(); 
                $response = ["success" => true, "code" => 200, "data" => ['ad_list' => $ad_details]];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionTnc()
    {
        $response = [];
        $header_response = AdminUser::verifyHeaders(Yii::$app->request->headers);
        if($header_response['code'] != 200){
            return $header_response;
        }

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            $tnc = Tnc::getTnc(); 
                $response = ["success" => true, "code" => 200, "data" => $tnc];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionTncAutoSave()
    {
        $response = [];
        $header_response = AdminUser::verifyHeaders(Yii::$app->request->headers);
        if($header_response['code'] != 200){
            return $header_response;
        }

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            $content = Tnc::autoSaveTnc($post); 
                $response = ["success" => true, "code" => 200, "data" => ['saved' => true,'content' => $content]];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionTncSave()
    {
        $response = [];
        $header_response = AdminUser::verifyHeaders(Yii::$app->request->headers);
        if($header_response['code'] != 200){
            return $header_response;
        }

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            $content = Tnc::saveTncForWebsite($post); 
                $response = ["success" => true, "code" => 200, "data" => ['saved' => true,'content' => $content]];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionAboutUs()
    {
        $response = [];
        $header_response = AdminUser::verifyHeaders(Yii::$app->request->headers);
        if($header_response['code'] != 200){
            return $header_response;
        }

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            $about_us = AboutUs::getAboutUs(); 
                $response = ["success" => true, "code" => 200, "data" => $about_us];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionAboutUsAutoSave()
    {
        $response = [];
        $header_response = AdminUser::verifyHeaders(Yii::$app->request->headers);
        if($header_response['code'] != 200){
            return $header_response;
        }

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            $content = AboutUs::autoSaveAboutUs($post); 
                $response = ["success" => true, "code" => 200, "data" => ['saved' => true,'content' => $content]];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionAboutUsSave()
    {
        $response = [];
        $header_response = AdminUser::verifyHeaders(Yii::$app->request->headers);
        if($header_response['code'] != 200){
            return $header_response;
        }

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            $content = AboutUs::saveAboutUsForWebsite($post); 
                $response = ["success" => true, "code" => 200, "data" => ['saved' => true,'content' => $content]];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];

        }
 
        return $response;
 
    }

    public function actionFetchEnquiries()
    {
        $response = [];
        $header_response = AdminUser::verifyHeaders(Yii::$app->request->headers);
        if($header_response['code'] != 200){
            return $header_response;
        }

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            $enquiry = WebsiteEnquiry::getEnquiry(); 
            $response = ["success" => true, "code" => 200, "data" => $enquiry];

            if(!empty($enquiry)) WebsiteEnquiry::updateEnquiry();

        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        
        return $response;
    
    }

    public function actionDeleteEnquiry()
    {
        $response = [];
        $header_response = AdminUser::verifyHeaders(Yii::$app->request->headers);
        if($header_response['code'] != 200){
            return $header_response;
        }

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            
            $enquiry = WebsiteEnquiry::deleteEnquiry($post); 
            $response = ["success" => true, "code" => 200, "data" => $enquiry];

        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }

        return $response;


    }

    public function actionGetCategories()
    {
        $response = [];
        $header_response = AdminUser::verifyHeaders(Yii::$app->request->headers);
        if($header_response['code'] != 200){
            return $header_response;
        }

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            $category_list = Categories::getCategories($post);
                $response = ["success" => true, "code" => 200, "data" => $category_list];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionSaveCategory(){

        $response = [];
        $header_response = AdminUser::verifyHeaders(Yii::$app->request->headers);
        
        if($header_response['code'] != 200){
            return $header_response;
        }

        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $request = Yii::$app->request;
            $model->file = UploadedFile::getInstanceByName('file');

            if ($model->file && $model->validate()) {
                $image_name = "http://localhost/ads/api/v1/web/".$model->file->baseName . '.' . $model->file->extension;                
                $result = $model->file->saveAs( $model->file->baseName . '.' . $model->file->extension);
            }

            $post = $request->post();
            
            if (!empty($result) && $result == true) {
                $post['category_image'] = $image_name;
            }
            
            try{   
                $articledetail = Categories::addCategory($post); 
                    $response = ["success" => true, "code" => 200, "data" => $articledetail];
            }
            catch(\Exception $e){
                $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            }

            return $response;
        
        }

    }

    public function actionUpdateCategory(){

        $response = [];
        $header_response = AdminUser::verifyHeaders(Yii::$app->request->headers);
        
        if($header_response['code'] != 200){
            return $header_response;
        }

        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $request = Yii::$app->request;
            $model->file = UploadedFile::getInstanceByName('file');

            if ($model->file && $model->validate()) {
                $image_name = "http://localhost/ads/api/v1/web/".$model->file->baseName . '.' . $model->file->extension;                
                $result = $model->file->saveAs( $model->file->baseName . '.' . $model->file->extension);
            }

            $post = $request->post();
            
            if(!empty($result) && $result == true){
                $post['category_image'] = $image_name;
            }
            
            try{   
                $articledetail = Categories::categoryUpdate($post); 
                    $response = ["success" => true, "code" => 200, "data" => $articledetail];
            }
            catch(\Exception $e){
                $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            }

            return $response;
        
        }

    }

    public function actionDeleteCategory()
    {
        $response = [];
        $header_response = AdminUser::verifyHeaders(Yii::$app->request->headers);
        if($header_response['code'] != 200){
            return $header_response;
        }

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            
            $category = Categories::categoryDelete($post); 
            $response = ["success" => true, "code" => 200, "data" => $category];

        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }

        return $response;
    }


    public function actionWebsiteUserBlockUnblock()
    {
        $response = [];
        $header_response = AdminUser::verifyHeaders(Yii::$app->request->headers);
        if($header_response['code'] != 200){
            return $header_response;
        }

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            
            $user_blocked = WebsiteUsers::userAccountBlockUnblock($post); 
            $response = ["success" => true, "code" => 200, "data" => $user_blocked];

        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }

        return $response;
    }

    public function actionWebsiteAdBlockUnblock()
    {
        $response = [];
        $header_response = AdminUser::verifyHeaders(Yii::$app->request->headers);
        if($header_response['code'] != 200){
            return $header_response;
        }

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            
            $user_un_blocked = AdDetail::websiteAdBlockUnBlock($post); 
            $response = ["success" => true, "code" => 200, "data" => []];

        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }

        return $response;
    }

}
