<?php
namespace api\v1\controllers;

use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;

use yii\web\Controller;
use yii\web\Response;

use common\models\WebsiteUsers;

use common\models\Tnc;

use common\models\SubscribeUs;
use common\models\WebsiteEnquiry;

use common\models\AboutUs;

use common\models\Categories;

use common\models\ArticleDetail;
use common\models\AdDetail;
use common\models\AdManagment;

use common\models\Notifications;

use common\models\Cities;
use common\models\States;

use yii\rest\ActiveController;
use Yii;
use yii\console\Exception;

use common\models\UploadForm;
use yii\web\UploadedFile;

use yii\base\Security;

/**
 * Auth controller

 */
class FrontendController extends ApiController
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'common\models\User';
    public function behaviors()
    {
        // $this->enableCsrfValidation = false;
        $behaviors = parent::behaviors();
        $behaviors['bootstrap'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                '' => ['post'],
            ],
        ];
        $behaviors['corsFilter']  = [
            'class' => \yii\filters\Cors::className(),
            'cors'  => [
                // Restrict access to
                'Access-Control-Allow-Origin' => ['*'],

                // restrict access to domains:
                'Origin' => ['http://backend.worldonrent.com', 'http://localhost/'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
                'Access-Control-Expose-Headers' => []
            ],
        ];

        return $behaviors;
    }

    /**
     * User Change Password.
     *
     * @return string
     */
    public function actionProfileChangePassword()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            $change_password = WebsiteUsers::changePasswordForFrontendController($post);
            
            $response = ["success" => true, "code" => 200, "data" => ['saved' => true,'data' => $change_password]];

            if(isset($change_password['Ack']) && ($change_password['Ack'] == 'Error'))
                $response = ["success" => false, 'errors' => ['message' => $change_password['Message'], "code" => 401]];

        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    /**
     * User Change Password.
     *
     * @return string
     */
    public function actionAccountProfileUpdate()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            $account_profile_update = WebsiteUsers::accountProfileUpdate($post);
                $response = ["success" => true, "code" => 200, "data" => ['saved' => true,'data' => $account_profile_update]];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    /**
     * User Account Deactive.
     *
     * @return string
     */
    public function actionAccountDeactive()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            $change_password = WebsiteUsers::accountDeactive($post);
                $response = ["success" => true, "code" => 200, "data" => ['saved' => true,'data' => $change_password]];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    /**
     * User Logout.
     *
     * @return string
     */
    public function actionAccountLogout()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            $account_logout = WebsiteUsers::accountLogout($post);
                $response = ["success" => true, "code" => 200, "data" => ['saved' => true,'data' => $account_logout]];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionGetCities()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            $city_list = Cities::getCities($post);
                $response = ["success" => true, "code" => 200, "data" => ['saved' => true,'data' => $city_list]];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionGetCitiesFilteredByName()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        
        try{   
            $city_list = Cities::getCitiesFilteredByName($post);
                $response = ["success" => true, "code" => 200, "data" => $city_list];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionGetCitiesFilteredById()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            $city_list = Cities::getCitiesFilteredByID($post);
                $response = ["success" => true, "code" => 200, "data" => $city_list];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }


    public function actionGetStates()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);
        try{   
            $state_list = States::getStates($post);
                $response = ["success" => true, "code" => 200, "data" => ['saved' => true,'data' => $state_list]];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionEnquirySave()
    {
        $response = [];
        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            $content = WebsiteEnquiry::getEnquirySave($post); 
                $response = ["success" => true, "code" => 200, "data" => ['saved' => true,'content' => $content]];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
        
    }


    public function actionSubscribeUsSave()
    {
        $response = [];
        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            $email = SubscribeUs::putSubscribeUsSave($post); 
                $response = ["success" => true, "code" => 200, "data" => ['saved' => true,'email' => $email]];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionTnc()
    {
        $response = [];
        
        try{   
            $tnc = Tnc::getTnc(); 
                $response = ["success" => true, "code" => 200, "data" => $tnc];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionAboutUs()
    {
        $response = [];
        
        try{   
            $about_us = AboutUs::getAboutUs(); 
                $response = ["success" => true, "code" => 200, "data" => $about_us];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionGetCategories()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            $category_list = Categories::getCategories($post);
                $response = ["success" => true, "code" => 200, "data" => ['saved' => true,'data' => $category_list]];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            //$response = ["success" => false, 'errors' => ['message' => \Yii::t('app', 'Sorry! Unknown error occurred. Please try again later.'), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionAdList()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            $ad_list = AdDetail::getListForCustomer($post); 
                $response = ["success" => true, "code" => 200, "data" => $ad_list];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionParentCategoryAdList()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            $ad_list = AdDetail::getAdListForParentCategory($post); 
                $response = ["success" => true, "code" => 200, "data" => $ad_list];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionAdSuggestionList()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            $ad_list = AdDetail::getSuggestionListForCustomer($post); 
                $response = ["success" => true, "code" => 200, "data" => $ad_list];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionAdDetails()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            $ad_details = AdDetail::getDetails($post);
                $response = ["success" => true, "code" => 200, "data" => $ad_details];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionAdOrArticleDetailsForEdit()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            $ad_details = AdDetail::getDetailsForEdit($post);
                $response = ["success" => true, "code" => 200, "data" => $ad_details];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionUsersAdList()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            $ad_list = AdDetail::getUserAdsList($post); 
                $response = ["success" => true, "code" => 200, "data" => $ad_list];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionUserRequestedAdList()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            $requested_ad_list = AdDetail::getUserRequestedAdsList($post); 
                $response = ["success" => true, "code" => 200, "data" => $requested_ad_list];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionRequestListForSpecificAd()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            $request_list_for_specific_ad = AdDetail::getRequestListForSpecificAd($post); 
                $response = ["success" => true, "code" => 200, "data" => $request_list_for_specific_ad];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    /* see if user has previous request is pending, if yes update that else create new request*/
    public function actionAdRequest()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            $ad_request = AdManagment::putAdRequest($post); 
                $response = ["success" => true, "code" => 200, "data" => $ad_request];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    /* see if user has previous request is pending, if yes update that else create new request*/
    public function actionUpdateAdRequestStatus()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            $update_ad_request = AdManagment::updateAdRequestStatus($post); 
                $response = ["success" => true, "code" => 200, "data" => $update_ad_request];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    /* Ad Deactivate */
    public function actionActivateDeactivateAd()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            $ad_request = AdDetail::putAdDeactive($post); 
                $response = ["success" => true, "code" => 200, "data" => $ad_request];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    /* Ad Soft Delete */
    public function actionDeleteSpecificAd()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            $ad_del = AdDetail::putAdDelete($post); 
                $response = ["success" => true, "code" => 200, "data" => $ad_del];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    /* Notifications */
    public function actionGetNotifications()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            
            $notifications = Notifications::getNotifications($post);

            $response = ["success" => true, "code" => 200, "data" => $notifications];
            
            $mark_read_notifications = Notifications::markReadNotification($post);

        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    /* Notifications */
    public function actionFetchUserDetails()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            $userInfo = WebsiteUsers::fetchUserDetails($post); 
                $response = ["success" => true, "code" => 200, "data" => $userInfo];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionRequestsResolvedByRentor()
    {
        $response = [];

        $post   = file_get_contents("php://input");
        $post   = json_decode($post, true);

        try{   
            $requested_ad_list = AdDetail::getRequestsResolvedByRentor($post); 
                $response = ["success" => true, "code" => 200, "data" => $requested_ad_list];
        }
        catch(\Exception $e){
            $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
        }
        return $response;
    }

    public function actionSaveAd(){

        $response = [];

        $model = new UploadForm();
        $model2 = new UploadForm();
        $model3 = new UploadForm();

        if (Yii::$app->request->isPost) {
            $request = Yii::$app->request;

            $security = new Security();

            $model->file = UploadedFile::getInstanceByName('file1');
            $model2->file = UploadedFile::getInstanceByName('file2');
            $model3->file = UploadedFile::getInstanceByName('file3');

            $image_name1 = '';
            $image_name2 = '';
            $image_name3 = '';
            $result1 = false;
            $result2 = false;
            $result3 = false;

            if ($model->file && $model->validate()) {
                $tempName = $security->generateRandomString(32);
                $image_name1 = $tempName . '.' . $model->file->extension;                
                $result1 = $model->file->saveAs( $tempName . '.' . $model->file->extension);
            }

            if ($model2->file && $model2->validate()) {
                $tempName = $security->generateRandomString(28);
                $image_name2 = $tempName . '.' . $model2->file->extension;                
                $result2 = $model2->file->saveAs( $tempName . '.' . $model2->file->extension);
            }

            if ($model3->file && $model3->validate()) {
                $tempName = $security->generateRandomString(24);
                $image_name3 = $tempName . '.' . $model3->file->extension;                
                $result3 = $model3->file->saveAs( $tempName . '.' . $model3->file->extension);
            }

            $post = $request->post();
            // return $this->render('upload', ['model' => $model]);
            
            if($result1 == true || $result3 == true || $result2 == true){
                $imageSrcArray = array();
                if ($result1 == true) {
                    array_push($imageSrcArray, $image_name1);
                }
                if ($result2 == true) {
                    array_push($imageSrcArray, $image_name2);
                }
                if ($result3 == true) {
                    array_push($imageSrcArray, $image_name3);
                }
                $post['img_src_payload'] = $imageSrcArray;
            }
            
            try{   
                $articledetail = ArticleDetail::articleDetailSave($post); 
                    $response = ["success" => true, "code" => 200, "data" => $articledetail];
            }
            catch(\Exception $e){
                $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            }

            return $response;
        
        }

    }

    public function actionUpdateArticleOrAd(){

        $response = [];

        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $request = Yii::$app->request;
            // $model->file = UploadedFile::getInstance($model, 'file');
            $model->file = UploadedFile::getInstanceByName('file');

            if ($model->file && $model->validate()) {
                $image_name = "http://localhost/ads/api/v1/web/".$model->file->baseName . '.' . $model->file->extension;                
                $result = $model->file->saveAs( $model->file->baseName . '.' . $model->file->extension);
            }

            $post = $request->post();
            
            if($result == true){
                $post['img_src_payload'] = $image_name;
            }
            
            try{   
                $articledetail = ArticleDetail::articleDetailUpdate($post); 
                    $response = ["success" => true, "code" => 200, "data" => $articledetail];
            }
            catch(\Exception $e){
                $response = ["success" => false, 'errors' => ['message' => $e->getMessage(), "code" => $e->getCode()]];
            }

            return $response;
        
        }

    }

    public function actionUpload()
    {
        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            // $model->file = UploadedFile::getInstance($model, 'file');
            $model->file = UploadedFile::getInstanceByName('file');

            if ($model->file && $model->validate()) {                
                $result = $model->file->saveAs( $model->file->baseName . '.' . $model->file->extension);
            }
        }

        // return $this->render('upload', ['model' => $model]);
        return $result;
    }
}
