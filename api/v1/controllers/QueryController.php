<?php
namespace api\v1\controllers;

use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use common\models\WebsiteUsers;
use common\models\AdminUser;
use common\models\AdDetail;
use common\models\Tnc;
use common\models\AboutUs;
use common\models\WebsiteEnquiry;
use yii\rest\ActiveController;
use Yii;
use yii\console\Exception;
/**
 * Auth controller

 */
class QueryController extends ApiController
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'common\models\User';
    public function behaviors()
    {
        // $this->enableCsrfValidation = false;
        $behaviors = parent::behaviors();
        $behaviors['bootstrap'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                '' => ['post'],
            ],
        ];
        // For cross-domain AJAX request
        $behaviors['corsFilter']  = [
            'class' => \yii\filters\Cors::className(),
            'cors'  => [
                // Restrict access to
                'Access-Control-Allow-Origin' => ['*'],

                // restrict access to domains:
                'Origin' => ['http://backend.worldonrent.com', 'http://localhost/'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
                'Access-Control-Expose-Headers' => []
            ],
        ];

        return $behaviors;
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionQueries()
    {
        $fetch_ad_details = ['
            SELECT
              tbl_ad_detail.id,
              tbl_ad_detail.article_id,
              tbl_ad_detail.user_id,
              tbl_ad_detail.ad_status,
              tbl_ad_detail.ad_created_on,
              tbl_ad_detail.total_views,
              tbl_article_detail.item_name,
              tbl_article_detail.item_category_id,
              tbl_article_detail.item_parent_category_id,
              tbl_article_detail.item_description,
              tbl_article_detail.item_city_id,
              tbl_article_detail.item_state_id,
              tbl_article_detail.item_rent_per_day,
              tbl_article_detail.item_rent_per_week,
              tbl_article_detail.item_rent_per_hour,
              tbl_article_detail.item_rent_per_monthly,
              tbl_article_detail.item_rent_per_year,
              tbl_article_detail.security_deposit,
              tbl_article_detail.quantity,
              tbl_article_detail.item_special_instruction,
              tbl_article_detail.img_src_payload,
              tbl_website_user.user_name,
              tbl_website_user.user_email,
              tbl_website_user.user_phone,
              tbl_website_user.user_profile_img,
              tbl_cities.name,
              states.name,
              countries.name,
              categories.category_name,
              parentCategories.category_name
            FROM
              (
              SELECT
                *
              FROM
                ad_detail
              WHERE
                id = 3 AND ad_deactivated = 0 AND ad_blocked = 0
            ) tbl_ad_detail
            JOIN
              article_detail AS tbl_article_detail ON tbl_ad_detail.article_id = tbl_article_detail.id
            JOIN
              (
              SELECT
                *
              FROM
                website_users
              WHERE
                user_blocked = 0 AND user_deactivated = 0
            ) tbl_website_user ON tbl_ad_detail.user_id = tbl_website_user.id
            JOIN
              cities AS tbl_cities ON tbl_cities.id = tbl_article_detail.item_city_id
            JOIN
              states ON states.id = tbl_article_detail.item_state_id
            JOIN
              countries ON countries.id = tbl_article_detail.item_country_id
            JOIN
              categories ON categories.id = tbl_article_detail.item_category_id
            JOIN
              categories parentCategories ON parentCategories.id = tbl_article_detail.item_parent_category_id
        '];


        $FETCH_ADS_LIST = ['
            SELECT
              tbl_ad_detail.id AS pk_ad_id,
              tbl_ad_detail.article_id,
              tbl_ad_detail.user_id,
              tbl_ad_detail.ad_status,
              tbl_ad_detail.ad_created_on,
              tbl_ad_detail.total_views,
              article_detail.id AS pk_article_id,
              article_detail.item_name,
              article_detail.item_category_id,
              article_detail.item_parent_category_id,
              article_detail.item_description,
              article_detail.item_city_id,
              article_detail.item_state_id,
              article_detail.item_rent_per_day,
              article_detail.item_rent_per_week,
              article_detail.item_rent_per_hour,
              article_detail.item_rent_per_monthly,
              article_detail.item_rent_per_year,
              article_detail.security_deposit,
              article_detail.quantity,
              article_detail.item_special_instruction,
              article_detail.img_src_payload,
              tbl_website_user.user_name,
              tbl_website_user.user_email,
              tbl_website_user.user_phone,
              tbl_website_user.user_profile_img,
              cities.name,
              states.name,
              countries.name,
              categories.category_name,
              parentCategories.category_name
            FROM
              (
              SELECT
                *
              FROM
                ad_detail
              WHERE
                ad_deactivated = 0 AND ad_blocked = 0
            ) tbl_ad_detail
            JOIN
              article_detail ON tbl_ad_detail.article_id = article_detail.id
            JOIN
              (
              SELECT
                *
              FROM
                website_users
              WHERE
                user_blocked = 0 AND user_deactivated = 0
            ) tbl_website_user ON tbl_ad_detail.user_id = tbl_website_user.id
            JOIN
              cities ON cities.id = article_detail.item_city_id
            JOIN
              states ON states.id = article_detail.item_state_id
            JOIN
              countries ON countries.id = article_detail.item_country_id
            JOIN
              categories ON categories.id = article_detail.item_category_id
            JOIN
              categories parentCategories ON parentCategories.id = article_detail.item_parent_category_id
        '];

    $FETCH_ADS_LIST_WHEN_FILTERS_ARE_PRESENT = ['
        SELECT
          cities.name,
          states.name,
          countries.name,

          categories.category_name,
          parentCategories.category_name,

          tbl_ad_detail.id AS pk_ad_id,
          
          article_detail.id AS pk_article_id,
          article_detail.item_name,
          article_detail.item_category_id,
          article_detail.item_parent_category_id,
          article_detail.item_description,
          article_detail.item_city_id,
          article_detail.item_state_id,
          article_detail.item_rent_per_day,
          article_detail.item_rent_per_week,
          article_detail.item_rent_per_hour,
          article_detail.item_rent_per_monthly,
          article_detail.item_rent_per_year,
          article_detail.security_deposit,
          article_detail.quantity,
          article_detail.item_special_instruction,
          article_detail.img_src_payload,
          tbl_website_user.user_name,
          tbl_website_user.user_email,
          tbl_website_user.user_phone,
          tbl_website_user.user_profile_img,

          tbl_ad_detail.article_id,
          tbl_ad_detail.user_id,
          tbl_ad_detail.ad_status,
          tbl_ad_detail.ad_created_on,
          tbl_ad_detail.total_views
        FROM
          (
          SELECT
            *
          FROM
            ad_detail
          WHERE
            ad_deactivated = 0 AND ad_blocked = 0
        ) tbl_ad_detail
        JOIN
          article_detail ON tbl_ad_detail.article_id = article_detail.id AND article_detail.item_name LIKE '%3BHK%'
        JOIN
          (
          SELECT
            *
          FROM
            website_users
          WHERE
            user_blocked = 0 AND user_deactivated = 0
        ) tbl_website_user ON tbl_ad_detail.user_id = tbl_website_user.id
        JOIN
          cities ON cities.id = article_detail.item_city_id AND cities.id = 3308
        JOIN
          states ON states.id = article_detail.item_state_id
        JOIN
          countries ON countries.id = article_detail.item_country_id
        JOIN
          categories ON categories.id = article_detail.item_category_id AND categories.id = 2
        JOIN
          categories parentCategories ON parentCategories.id = article_detail.item_parent_category_id
        LIMIT 1, 1
    ']

    $FETCH_CATEGORIES = ['
        SELECT
          child_category.id,
          child_category.category_name,
          child_category.category_desc,
          child_category.parent_category_id,
          child_category.date_created,
          child_category.date_last_modified,
          child_category.total_ads,
          child_category.total_subcategories,
          child_category.category_icon,
          parent_category.category_name
        FROM
          categories child_category
        LEFT JOIN
          categories parent_category ON child_category.parent_category_id = parent_category.id



        // OPTIONAL
        GROUP BY
          child_category.parent_category_id,
          child_category.category_name
    ']; 

    $FETCH_REQUEST_FOR_ADS__FETCH_ALL_REQUEST_FOR_A_SPECIFIC_AD = ['
        SELECT
          tbl_ad_managment.id AS ad_managment_id,
          tbl_ad_managment.rentor_id,
          tbl_ad_managment.rentee_id,
          tbl_ad_managment.ad_id,
          tbl_ad_managment.request_status,
          tbl_ad_managment.date_requested,
          tbl_ad_managment.date_resolved,
          tbl_ad_managment.is_ad_blocked,
          tbl_ad_managment.is_ad_deleted,
          tbl_ad_detail.id AS pk_ad_id,
          tbl_ad_detail.article_id,
          tbl_ad_detail.user_id,
          tbl_ad_detail.ad_status,
          tbl_ad_detail.ad_created_on,
          tbl_ad_detail.total_views,
          tbl_website_user.user_name,
          tbl_website_user.user_email,
          tbl_website_user.user_phone,
          tbl_website_user.user_profile_img,
          article_detail.id AS pk_article_id,
          article_detail.item_name,
          article_detail.item_category_id,
          article_detail.item_parent_category_id,
          article_detail.item_description,
          article_detail.item_city_id,
          article_detail.item_state_id,
          article_detail.item_rent_per_day,
          article_detail.item_rent_per_week,
          article_detail.item_rent_per_hour,
          article_detail.item_rent_per_monthly,
          article_detail.item_rent_per_year,
          article_detail.security_deposit,
          article_detail.quantity,
          article_detail.item_special_instruction,
          article_detail.img_src_payload,
          cities.name,
          states.name,
          countries.name,
          categories.category_name,
          parentCategories.category_name
        FROM
          (
          SELECT
            *
          FROM
            ad_managment
          WHERE
            rentor_id = 1 AND ad_id = 3 AND is_ad_blocked = 0 AND is_ad_deleted = 0
        ) AS tbl_ad_managment
        JOIN
          (
          SELECT
            *
          FROM
            ad_detail
          WHERE
            is_ad_deleted = 0 AND ad_deactivated = 0 AND ad_blocked = 0
        ) tbl_ad_detail ON tbl_ad_detail.id = tbl_ad_managment.ad_id
        JOIN
          article_detail ON tbl_ad_detail.article_id = article_detail.id
        JOIN
          (
          SELECT
            *
          FROM
            website_users
          WHERE
            user_blocked = 0 AND user_deactivated = 0
        ) tbl_website_user ON tbl_ad_managment.rentee_id = tbl_website_user.id
        JOIN
          cities ON cities.id = tbl_website_user.user_city_id
        JOIN
          states ON states.id = tbl_website_user.user_state_id
        JOIN
          countries ON countries.id = tbl_website_user.user_country_id
        JOIN
          categories ON categories.id = article_detail.item_category_id
        JOIN
          categories parentCategories ON parentCategories.id = article_detail.item_parent_category_id
    '];

    $user_Ads = [
      SELECT
        tbl_website_user.user_name,
        article_detail.item_name,
        tbl_website_user.user_email,
        tbl_website_user.user_phone,
        tbl_website_user.user_profile_img,
        
        article_detail.id AS pk_article_id,
        article_detail.item_category_id,
        article_detail.item_parent_category_id,
        article_detail.item_description,
        article_detail.item_city_id,
        article_detail.item_state_id,
        article_detail.item_rent_per_day,
        article_detail.item_rent_per_week,
        article_detail.item_rent_per_hour,
        article_detail.item_rent_per_monthly,
        article_detail.item_rent_per_year,
        article_detail.security_deposit,
        article_detail.quantity,
        article_detail.item_special_instruction,
        article_detail.img_src_payload,

        cities.name,
        states.name,
        countries.name,

        categories.category_name,
        parentCategories.category_name,

        tbl_ad_detail.id AS pk_ad_id,

        tbl_ad_detail.article_id,
        tbl_ad_detail.user_id,
        tbl_ad_detail.ad_status,
        tbl_ad_detail.ad_created_on,
        tbl_ad_detail.total_views
      FROM
        (
        SELECT
          *
        FROM
          ad_detail
        WHERE
          ad_deactivated = 0 AND ad_blocked = 0
      ) tbl_ad_detail
      JOIN
        article_detail ON tbl_ad_detail.article_id = article_detail.id
      JOIN
        (
        SELECT
          *
        FROM
          website_users
        WHERE
          user_blocked = 0 AND user_deactivated = 0
      ) tbl_website_user ON tbl_ad_detail.user_id = tbl_website_user.id AND tbl_ad_detail.user_id = 2
      JOIN
        cities ON cities.id = article_detail.item_city_id
      JOIN
        states ON states.id = article_detail.item_state_id
      JOIN
        countries ON countries.id = article_detail.item_country_id
      JOIN
        categories ON categories.id = article_detail.item_category_id
      JOIN
        categories parentCategories ON parentCategories.id = article_detail.item_parent_category_id
    ];

    $FETCH_ALL_ADS_REQUESTED_BY_USER = [
    SELECT
      tbl_ad_managment.id AS ad_managment_id,
      tbl_ad_managment.rentor_id,
      tbl_ad_managment.rentee_id,
      tbl_ad_managment.ad_id,
      tbl_ad_managment.request_status,
      tbl_ad_managment.date_requested,
      tbl_ad_managment.date_resolved,
      tbl_ad_managment.is_ad_blocked,
      tbl_ad_managment.is_ad_deleted,
      tbl_ad_detail.id AS pk_ad_id,
      tbl_ad_detail.article_id,
      tbl_ad_detail.user_id,
      tbl_ad_detail.ad_status,
      tbl_ad_detail.ad_created_on,
      tbl_ad_detail.total_views,
      tbl_website_user.user_name,
      tbl_website_user.user_email,
      tbl_website_user.user_phone,
      tbl_website_user.user_profile_img,
      article_detail.id AS pk_article_id,
      article_detail.item_name,
      article_detail.item_category_id,
      article_detail.item_parent_category_id,
      article_detail.item_description,
      article_detail.item_city_id,
      article_detail.item_state_id,
      article_detail.item_rent_per_day,
      article_detail.item_rent_per_week,
      article_detail.item_rent_per_hour,
      article_detail.item_rent_per_monthly,
      article_detail.item_rent_per_year,
      article_detail.security_deposit,
      article_detail.quantity,
      article_detail.item_special_instruction,
      article_detail.img_src_payload,
      cities.name,
      states.name,
      countries.name,
      categories.category_name,
      parentCategories.category_name
    FROM
      (
      SELECT
        *
      FROM
        ad_managment
      WHERE
        rentee_id = 2
    ) AS tbl_ad_managment
    JOIN
      (
    SELECT
      *
    FROM
      ad_detail
    ) tbl_ad_detail ON tbl_ad_detail.id = tbl_ad_managment.ad_id
    JOIN
      article_detail ON tbl_ad_detail.article_id = article_detail.id
    JOIN
      (
      SELECT
        *
      FROM
        website_users
      WHERE
        user_blocked = 0 AND user_deactivated = 0
    ) tbl_website_user ON tbl_ad_detail.user_id = tbl_website_user.id
    JOIN
      cities ON cities.id = article_detail.item_city_id
    JOIN
      states ON states.id = article_detail.item_state_id
    JOIN
      countries ON countries.id = article_detail.item_country_id
    JOIN
      categories ON categories.id = article_detail.item_category_id
    JOIN
      categories parentCategories ON parentCategories.id = article_detail.item_parent_category_id
    ];

    $FETCH_ALL_REQUEST_FOR_A_SPECIFIC_AD = [
      SELECT
        tbl_ad_managment.id AS ad_managment_id,
        tbl_ad_managment.rentor_id,
        tbl_ad_managment.rentee_id,
        tbl_ad_managment.ad_id,
        tbl_ad_managment.request_status,
        tbl_ad_managment.date_requested,
        tbl_ad_managment.date_resolved,
        tbl_ad_managment.is_ad_blocked,
        tbl_ad_managment.is_ad_deleted,
        tbl_ad_detail.id AS pk_ad_id,
        tbl_ad_detail.article_id,
        tbl_ad_detail.user_id,
        tbl_ad_detail.ad_status,
        tbl_ad_detail.ad_created_on,
        tbl_ad_detail.total_views,
        tbl_website_user.user_name,
        tbl_website_user.user_email,
        tbl_website_user.user_phone,
        tbl_website_user.user_profile_img,
        article_detail.id AS pk_article_id,
        article_detail.item_name,
        article_detail.item_category_id,
        article_detail.item_parent_category_id,
        article_detail.item_description,
        article_detail.item_city_id,
        article_detail.item_state_id,
        article_detail.item_rent_per_day,
        article_detail.item_rent_per_week,
        article_detail.item_rent_per_hour,
        article_detail.item_rent_per_monthly,
        article_detail.item_rent_per_year,
        article_detail.security_deposit,
        article_detail.quantity,
        article_detail.item_special_instruction,
        article_detail.img_src_payload,
        cities.name,
        states.name,
        countries.name,
        categories.category_name,
        parentCategories.category_name
      FROM
        (
        SELECT
          *
        FROM
          ad_managment
        WHERE
          rentor_id = 1 AND ad_id = 3 AND is_ad_blocked = 0 AND is_ad_deleted = 0
      ) AS tbl_ad_managment
      JOIN
        (
        SELECT
          *
        FROM
          ad_detail
        WHERE
          is_ad_deleted = 0 AND ad_deactivated = 0 AND ad_blocked = 0
      ) tbl_ad_detail ON tbl_ad_detail.id = tbl_ad_managment.ad_id
      JOIN
        article_detail ON tbl_ad_detail.article_id = article_detail.id
      JOIN
        (
        SELECT
          *
        FROM
          website_users
        WHERE
          user_blocked = 0 AND user_deactivated = 0
      ) tbl_website_user ON tbl_ad_managment.rentee_id = tbl_website_user.id
      JOIN
        cities ON cities.id = tbl_website_user.user_city_id
      JOIN
        states ON states.id = tbl_website_user.user_state_id
      JOIN
        countries ON countries.id = tbl_website_user.user_country_id
      JOIN
        categories ON categories.id = article_detail.item_category_id
      JOIN
        categories parentCategories ON parentCategories.id = article_detail.item_parent_category_id
    ];


    }
}
