<?php

namespace api\models;
use webvimark\modules\UserManagement\models\User as BaseUser;
use yii\helpers\ArrayHelper;
use yii;

class User extends BaseUser
{

    /**
     * @inheritdoc
     */
    public $roles;
    //public $events;
    const ROLE_RKCL_ADMIN = 'Admin';
    const ROLE_ESAKHI = 'esakhi';

     public function behaviors() {
        return parent::behaviors();
    }
    public function rules()
    {
        return parent::rules();
    }
    public function getIsSuperAdmin(){
        if(isset(Yii::$app->user->identity->superadmin) && Yii::$app->user->identity->superadmin==1){
            return true;
        }
        return false;
    }
}