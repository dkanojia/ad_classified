<?php

/**
 * Created by PhpStorm.
 * User: manjunath
 * Date: 2/11/15
 * Time: 4:32 PM
 */

namespace common\helpers;


use api\modules\v1\models\AppUser;
use api\modules\v1\helper\AppRequest;

class ErrorConstant {

    //status
    const STATUS_TRUE = 1;
    const STATUS_FASE = 0;
    const STATUS_OK = 200;
    const STATUS_INVALID_CREDENTIALS = 401;
    const STATUS_SERVER_ERROR = 500;
    const STATUS_FEMALE_ONLY = 403;
    const STATUS_INVALID_BHAMASHAH = 403;
    const STATUS_MODEL_ERROR = 500;
    const EMPTY_LOGIN = 401;
    const EMPTY_GCM = 401;
    const EMPTY_DETAILS = 401;
    const INVALID_USER = 401;
    const IDs_BLANK = 400;
    const CENTRE_ID_UPDATE = 402;
    const INVALID_OTP = 401;
    const STATUS_ALREADY_COMPLETED = 403;
    const STATUS_ALREADY_ADDED = 403;
    const STATUS_APP_HARD_UPDATE = 426;
    const STATUS_APP_SOFT_UPDATE = 425;
}

?>
