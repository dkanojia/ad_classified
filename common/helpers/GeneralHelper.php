<?php

namespace common\helpers;

use Yii;
use webvimark\modules\UserManagement\UserManagementModule;
use yii\helpers\Html;
use backend\models\User;

class GeneralHelper
{
   public static function sendpush_notification($registatoin_ids,$app_type,$message,$user_id)
   {
        if($app_type == 1){
            return self::send_push_notification_android($registatoin_ids,$message,$app_type,$user_id);
        }
        else if($app_type == 2) {
            return self::send_push_notification_ios($registatoin_ids,$message);
        }
        return false;
   }
   public static function send_push_notification_android($registatoin_ids, $message,$app_type,$user_id) {
      
       $url = 'https://android.googleapis.com/gcm/send';
      
       $fields = array(
           'registration_ids' => $registatoin_ids,
           'data' => array("message"=>$message),
       );
       //$fields = json_encode ( $fields );
       $headers = array(
           'Authorization: key=' . Yii::$app->params["googleApiKey"],
           'Content-Type: application/json'
       );
       
       // Open connection
       $ch = curl_init();

       // Set the url, number of POST vars, POST data
       curl_setopt($ch, CURLOPT_URL, $url);

       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

       // Disabling SSL Certificate support temporarly
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        
       // Execute post
       $result = curl_exec($ch);
       $status=1;
       if ($result === FALSE) {
        $status=0;
           //die('Curl failed: ' . curl_error($ch));
       }
       else
       {
            $res=json_decode($result,true);
            
            if($res['success']==1)
            {
                $status=1;
            }   
            else
            {
                $status=0;
            }   
       }    
       curl_close($ch);
      
       return $status;
    
       
   }
   public static function send_push_notification_ios($deviceToken,$message)
    {
      // Put your device token here (without spaces):
      //$deviceToken = '0f744707bebcf74f9b7c25d48e3358945f6aa01da5ddb387462c7eaf61bbad78';
      /**/
      $passphrase = Yii::$app->params["IOSPassPhrase"];
      $file = Yii::$app->params["IOSPemFile"];
      $url = Yii::$app->params["IOSUrl"];
      $status = 0;
      // Put your alert message here:
      //$message = 'My first push notification!';

      ////////////////////////////////////////////////////////////////////////////////

      try{
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', __DIR__.'/../config/'.$file);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
        $fp = stream_socket_client(
        $url, $err,
        $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)
        {
          $status = 0;
        }  
        else {
          // Create the payload body
          $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default'
            );

          // Encode the payload as JSON
          $payload = json_encode($body);

          // Build the binary notification
          $msg = chr(0) . pack('n', 32) . pack('H*', @$deviceToken[0]) . pack('n', strlen($payload)) . $payload;

          // Send it to the server
          $result = fwrite($fp, $msg, strlen($msg));
          fclose($fp); 
          if (!$result)
          {
            $status = 0;
          } 
          else
          {
            $status = 1;
          }  
        } 
      } catch(\Exception $e) {
        
      }        
      return $status;
    }
    public static function getBackendMenuItems()
    {
        
      if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/user/login'],'template' => '<a href="{url}"><em><svg class="icon icon-login_user"><use xlink:href="'.Yii::$app->request->baseUrl.'/img/svg/symbol-defs.svg#icon-login_user"></use></svg></em><span data-localize="sidebar.nav.LOGIN">
              {label}</span></a>'];
      } else {
        $menuItems = [
            ['label' => Yii::t("app",'Dashboard'), 'url' => ['/site/index'], 'template' => '<a href="{url}"><span data-localize="sidebar.nav.DASHBOARD">
              {label}</span></a>'],
        ];
       
      }
      return $menuItems;
    }
   
    public static function generateRandomString($length = 10) 
    {
        $chrList = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@#$';
       // Minimum/Maximum times to repeat character List to seed from
        $chrRepeatMin = 1; // Minimum times to repeat the seed string
        $chrRepeatMax = 10; // Maximum times to repeat the seed string

        
        // The ONE LINE random command with the above variables.
        return substr(str_shuffle(str_repeat($chrList, mt_rand($chrRepeatMin,$chrRepeatMax))),1,$length);
    }

    public static function tokenReplace($message,$variables)
    {
    	preg_match_all('/
    			\{             # [ - pattern start
    			([^\s\{\}:]*)  # match $type not containing whitespace : [ or ]
    			:              # : - separator
    			([^\{\}]*)     # match $name not containing [ or ]
    			\}             # ] - pattern end
    			/x', $message, $matches);

    	$types = $matches[1];
    	$tokens = $matches[2];

    	for($i=0;$i<count($types);$i++)
    	{
    		if(isset($variables['{'.$types[$i].'}'][$tokens[$i]]))
    		{
    			$search='{'.$types[$i].':'.$tokens[$i].'}';
    			$replace=$variables['{'.$types[$i].'}'][$tokens[$i]];
    			$message=str_replace($search,$replace,$message);
    			//unset($variables[$types[$i]]);
    		}
    	}
    	for($i=0;$i<count($types);$i++)
    	{
	    	if(isset($variables['{'.$types[$i].'}'][$tokens[$i]]))
	    	{
	    	  unset($variables['{'.$types[$i].'}']);
	    	}
    	}

    	$searchArray=array_keys($variables);
    	$replaceArray=array_values($variables);
    	return str_replace($searchArray,$replaceArray,$message);
    }
    public static function send_onesignalpush_notification($registratoin_id,$title, $message, $link="", $additional_data=array()) 
    {
      
         $content = array(
  			"en" => $message
  			);
         $heading = array(
  			"en" => $title
  			);

        $resp = [];
      $player_chunks = array_chunk($registratoin_id, 2000);

      $resp = [];

       foreach ($player_chunks as $key => $value) {
      		$fields = array(
            'app_id' => Yii::$app->params['onesignalAppId'],
            'include_player_ids' => $value,
            'contents' => $content,
            'headings' => $heading,
            'data' => (object)$additional_data,
          );

      		if(!empty($link)){
                  $fields['url'] = $link;
              }
      		$fields = json_encode($fields);
          	//print("\nJSON sent:\n");
          	//print($fields);
      		
      		$ch = curl_init();
      		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
      		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; ',
      												   'Authorization: '.Yii::$app->params['onesignalApiKey']));
              //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
      	    //                       'Authorization: Basic ODE3NGM5MjQtMDg0MC00MzQ1LTkwNmQtYTJmZGU0ODQ3Nzcy'));
      		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      		curl_setopt($ch, CURLOPT_HEADER, FALSE);
      		curl_setopt($ch, CURLOPT_POST, TRUE);
      		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
      		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

      		$response = curl_exec($ch);
      		curl_close($ch);
          $resp = (array)json_decode($response);
       }
      
      //var_dump($response);die;
      // var_dump($resp);die;
      // if(isset($resp['recipients'])){
      //     return 1;
      // }else{
      //     return 0;
      // }
      
  		return $resp;
    }
    
    public static function normalizeFilename($filename) 
    {
        $filename = strip_tags($filename); // Strip HTML Tags Remove Break/Tabs/Return Carriage

        $filename = preg_replace('/[\r\n\t ]+/', ' ', $filename);
        // Remove Illegal Chars for folder and filename

        $filename = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $filename);
        // Put the string in lower case

        $filename = strtolower($filename);

        /**
         * Remove foreign accents such as Éàû by convert it into html entities
         * and then remove the code and keep the letter.
         */
        $filename = html_entity_decode($filename, ENT_QUOTES, "utf-8");
        $filename = htmlentities($filename, ENT_QUOTES, "utf-8");
        $filename = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $filename);

        // Replace Spaces with dashes
        $filename = str_replace(' ', '-', $filename);

        /**
         * Encode special chars that could pass the previous steps
         * and enter in conflict filename on server.
         */
        $filename = rawurlencode($filename);
        /**
         * Replace "%" with dashes to make sure the link of the file will
         * not be rewritten by the browser when querying th file.
         */
        $filename = str_replace('%', '-', $filename);

        return $filename;
    }

    public static function curlRequestPost($url, $postData, $port = "80", $file_upload = false)
    {
      $curl = curl_init();

      $httpHeader = array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
          );
      if($file_upload) {
        $httpHeader = array(
            "cache-control: no-cache",
            'Content-Type: multipart/form-data',
          );
      } 

      curl_setopt_array($curl, array(
          // CURLOPT_PORT => $port,
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $postData,
          CURLOPT_HTTPHEADER => $httpHeader,
      ));

      $curlResponse = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);
      
      if (!$err) {
          return $curlResponse = json_decode($curlResponse, true);
      }
      return false;
  }

  public static function curlRequestGet($url, $port = "80")
    {
      $curl = curl_init();

      curl_setopt_array($curl, array(
          CURLOPT_PORT => $port,
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
          ),
      ));

      $curlResponse = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);
      
      if (!$err) {
          return $curlResponse = json_decode($curlResponse, true);
      }
      return false;
  }

  public static function sendOtp($mobile, $message)
  {
    $url = Yii::$app->params['smsUrl'];

    $postData = array(
        'username' => 'rajmsdg-elocker',
        'password' => 'eLocker@16',
        'smsservicetype' => 'singlemsg',
        'content' => $message,
        'mobileno' => $mobile,
        'senderid' => 'RAJSMS'
    );

    $postData = http_build_query($postData);

    return static::curlRequestPost($url, $postData);
  }

  public static function translateArray($value)
  {
      if(is_array($value)) {
        $new_array = [];
        foreach ($value as $key => $val) {
          $new_array[$key] = Yii::t('app', $val);
        }
        return $new_array;
      }
      return Yii::t('app', $value);
  }

  public static function ordinalSuffixOf($number)
  {
      $j = $number % 10;
      $k = $number % 100;
      if ($j == 1 && $k != 11) {
          return $number . "st";
      }
      if ($j == 2 && $k != 12) {
          return $number . "nd";
      }
      if ($j == 3 && $k != 13) {
          return $number . "rd";
      }
      return $number . "th";
  }

  public static function imageResizeQuality($src, $dst, $quality  = 100)
  {
        if (!list($w, $h) = getimagesize($src)) {
            return "Unsupported picture type!";
        }

        $type = strtolower(substr(strrchr($src, "."), 1));
        if ($type == 'jpeg') {
            $type = 'jpg';
        }
        switch ($type) {
        case 'jpg': $img = imagecreatefromjpeg($src); break;
        case 'png': $img = imagecreatefrompng($src); break;
        default: return "Unsupported picture type!";
      }

      // resize
      $width = $w;
      $height = $h;
      $x = 0;

      $new = imagecreatetruecolor($width, $height);

      // preserve transparency
      if ($type == "png") {
          imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
          imagealphablending($new, false);
          imagesavealpha($new, true);
      }

      imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);

      switch ($type) {
        case 'jpg': imagejpeg($new, $dst); break;
        case 'png': imagepng($new, $dst); break;
      }


        $source = $dst;
        $destination = $dst;


        $info = getimagesize($source);

        if ($info['mime'] == 'image/jpeg') {
            $image = imagecreatefromjpeg($source);
        } elseif ($info['mime'] == 'image/png') {
            $image = imagecreatefrompng($source);
        }

        imagejpeg($image, $destination, $quality);
        return true;
  }
}
?>