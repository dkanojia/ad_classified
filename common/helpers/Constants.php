<?php

namespace common\helpers;

use yii;
use common\models\ScheduleNotification;

class Constants {
  const SECRET_KEY = 'auriga123!@#';
  const OTP_EXPIRY_MINUTES = '5';
  const GREEN =  "598526";
  const LIGHT_GREEN =  "90c53f";
  const YELLOW =  "fccd3d";
  const STATUS_INACTIVE = 0;
  const STATUS_ACTIVE = 1;
  const STATUS_DELETED = 2;
  const EVERYONE = 0;
  const ATTENDEES = 1;
  const HIDE = 2;
  const STATUS_POLL_COMPLETED = 3;
  const DATA_NOT_AVAILABLE = -10;
  const OTP_MESSAGE = "Your otp is";
  const OTP_MIN = 1000;
  const OTP_MAX = 9999;
  const BANNER_MAXWIDTH = 768;
  const BANNER_MINWIDTH = 320;  
  const BANNER_MAXHEIGHT = 288;
  const BANNER_MINHEIGHT = 120;
  const BANNER_MAXSIZE = 500;//in KB  
  const BANNER_RATIO = 2.66;//8:3 with precision 2
  const EVENT_LOGO_RATIO = 1.77;
  const EVENT_LOGO_WIDTH = 500;
  const RULES_BANNER_MAXWIDTH = 768;
  const RULES_BANNER_MINWIDTH = 320;  
  const RULES_BANNER_MAXHEIGHT = 288;
  const RULES_BANNER_MINHEIGHT = 120;
  const RULES_BANNER_MAXSIZE = 500;//in KB  
  const RULES_BANNER_RATIO = 2.66;//8:3 with precision 2
  const BOOKMARK_SUCCESS_MESSAGE = "Session Bookmarked Successfully";
  const UNBOOKMARK_SUCCESS_MESSAGE = "Session Unbookmarked Successfully";  
  const NOTIFICATION_SESSION_CRON = "Session started after 15 min";
  const NOTIFICATION_POLL_NAME="Notification name for poll";
  const NOTIFICATION_FEEDBACK_NAME="Notification name for feedback";
  const NOTIFICATION_ASK_NAME="Notification name for ask";
  const CHECKIN_VERIFIED = "You have successfully verified.";
  const CHECKIN_NOTVERIFIED = "Currently not verified.";
  const ASK_SUBMITTED = "Your Question has been submitted successfully.";
  const PROFILE_PIC_WIDTH = 500;
  const PROFILE_PIC_RATIO = 1.77;
  public static function getStatus($code = null)
  {
    $_items = [
        self::STATUS_ACTIVE => Yii::t('app', 'Active'),
        self::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
        self::STATUS_DELETED => Yii::t('app', 'Deleted'),
    ];

    if (isset($code)) {
        return isset($_items[$code])? $_items[$code]:null;
    } else {
        unset($_items[self::STATUS_DELETED]);
        return $_items;
    }
  }
  public static function getPollStatus($code = null)
  {
    $_items = [
      self::STATUS_ACTIVE => Yii::t('app', 'Active'),
      self::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
      self::STATUS_DELETED => Yii::t('app', 'Deleted'),
      self::STATUS_POLL_COMPLETED => Yii::t('app', 'Completed'),
    ];

    if(isset($code)){
      return isset($_items[$code])?$_items[$code]:null;
    }
    else{
      unset($_items[self::STATUS_DELETED]);
      return $_items;
    }
  }

  public static function getPriority()
  {
    return [
      "1" => "1",
      "2" => "2",
      "3" => "3",
      "4" => "4",
      "5" => "5",
    ];
  }
  public static function getEntityVariables($entityType){
        if($entityType == ScheduleNotification::ENTITY_SESSION){
            $variables = ['{sessionName}' => 'For Session Name', 
                '{sessionStartTime}' => 'For Session Start time', 
                '{sessionEndTime}' => 'For Session End Time'
                ];
        }else if($entityType == ScheduleNotification::ENTITY_POLL){
            $variables = ['{pollName}'=>'For Poll Name',
                '{pollStartTime}'=>'For Poll Start time',
                '{pollEndTime}'=>'For Poll End time'];
        }else if($entityType == ScheduleNotification::ENTITY_FEEDBACK){
            $variables = ['{feedbackName}'=>'For Feedback Name', '{feedbackStartTime}'=>'For feedback Start time', 
                '{feedbackEndTime}'=>'For feedback end time'];
        }else if($entityType == ScheduleNotification::ENTITY_WEBCAST){
            $variables = ['{webcastName}'=>'For webcast name'];
        }else if($entityType == ScheduleNotification::ENTITY_GALLERY){
            $variables = ['{galleryName}'=>'For Gallery Name'];
        }else if($entityType == ScheduleNotification::ENTITY_GAME){
            $variables = ['{gameName}' => 'For Game Name'];
        }else if($entityType == ScheduleNotification::ENTITY_STATICPAGE){
            $variables = ['{pageTitle}' => 'For Page Title'];
        }
        $variables['{username}'] = 'For user name';
        return $variables;
    }
    public static function getColors(){
      return [
          'primary_color' => '#4b4590',
          'secondary_color' => '#24abe8',
          'primary_font_color' => '#000000',
          'body_color' => '#000000',
          'brand_font_color1' => '#4b4590',
          'brand_font_color2' => '#24abe8',
          'highlight_color' => '#a3a3a3',
          'background_color1' => '#4b4590',
          'background_color2' => '#24abe8',
          'header_bg_color' => '#4b4590',
          'header_font_color' => '#ffffff',
          'button_bg_color' => '#000000',
          'button_font_color' => '#ffffff',
      ];
    }
    public static function getFontSizes(){
      return [
          'triple_small_font' => '9',
          'double_small_font' => '10',
          'extra_small_font' => '11',
          'small_font' => '13',
          'base_font' => '12',
          'medium_font' => '14',
          'large_font' => '15',
          'extra_large_font' => '16',
          'double_large_font' => '17',
          'triple_large_font' => '18',    
      ];
    }
}
