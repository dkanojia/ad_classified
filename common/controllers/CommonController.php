<?php

namespace common\controllers;

use Yii;
use yii\web\Controller;
use common\helpers\Helper;
use backend\models\User;

class CommonController extends Controller
{

    //Define RBAC access for controller (If needs to skip this behavior - use 
    //your own behaviors methode in target controller)
    public function behaviors()
    {
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    public function init()
    {
        Yii::$container->set('yii\data\Pagination', [
               'pageSizeLimit' => isset($_GET['pageSize']) ?  $_GET['pageSize'] : Yii::$app->params['defaultPageSize'],
               'defaultPageSize' => Yii::$app->params['defaultPageSize'],
        ]);

        parent::init();
    }

    public function beforeAction($action)
    {
      \Yii::$app->language = \Yii::$app->session['_lang'];
      //return true;
      return parent::beforeAction($action);
    }

    public function actionBulkDelete($ids, $item_name = NULL)
    {
    	$ids = explode( ',', $ids );
        $item_name = $item_name ? $item_name : 'Items';
        foreach($ids as $id ){
            $model = $this->findModel($id);
        	$model->setAsDeleted();
        	$model->save(false);
        }
        Helper::setActionMessage(Yii::t("app", "Selected ".$item_name." have been deleted successfully"), 's');
        return $this->redirect(['index']);
    }

    public function actionBulkEnable($ids, $item_name = NULL)
    {
    	$ids = explode( ',', $ids );
        $item_name = $item_name ? $item_name : 'Items';
        foreach($ids as $id ){
            $model = $this->findModel($id);
        	$model->setAsEnabled();
        	$model->save(false);
        }
        Helper::setActionMessage(Yii::t("app", "Selected ".$item_name." have been enabled successfully"), 's');
        return $this->redirect(['index']);
    }

    public function actionBulkDisable($ids, $item_name = NULL)
    {
    	$ids = explode( ',', $ids );
        $item_name = $item_name ? $item_name : 'Items';
        foreach($ids as $id ){
            $model = $this->findModel($id);
        	$model->setAsDisabled();
        	$model->save(false);
        }
        Helper::setActionMessage(Yii::t("app", "Selected ".$item_name." have been disabled successfully"), 's');
        return $this->redirect(['index']);
    }

    //condition redirect
    protected function conRedirect($url)
    {
        if(!empty(Yii::$app->request->referrer)) {
          return $this->redirect(Yii::$app->request->referrer);
        }
        else {
          return $this->redirect($url);
        }
    }

    public function actionDisableStatus($id){
        $m = $_GET['m'];
        $model = m::findOne($id);
        $model->status = m::STATUS_INACTIVE;
        $model->save();
        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionEnableStatus($id){
        $m = $_GET['m'];
        $model = m::findOne($id);
        $model->status = m::STATUS_ACTIVE;
        $model->save();
        return $this->redirect(Yii::$app->request->referrer);
    }
}