<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'defaultPageSize' => 20,
     'pageSizes' => [2 => 2, 10 => 10, 20 => 20, 50 => 50, 100 => 100, 200 => 200,'all' => 'All']
];
