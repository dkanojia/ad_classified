<?php
namespace common\behaviors;

use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\db\Expression;
use Yii;

class ARBlameableBehavior extends Behavior
{

    public function events()
    {
        return [
            // ActiveRecord::EVENT_BEFORE_VALIDATE =>'beforeValidate',
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsert',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdate',
        ];
    }

    public function beforeValidate($event)
    {
        $model = $event->sender;
        if($model->isNewRecord)
          $this->beforeInsert($event);
        else
          $this->beforeUpdate($event);
    }


    public function beforeInsert($event)
    {
        $model = $event->sender;
        if(!Yii::$app instanceof Yii\console\Application){
            if ($model->hasAttribute('created_by') && is_null($model->created_by)) {
                $model->created_by = Yii::$app->user->id;
            }
            if ($model->hasAttribute('updated_by') && is_null($model->updated_by)) {
                $model->updated_by = Yii::$app->user->id;
            }
        }
    }

    public function beforeUpdate($event)
    {
        $model = $event->sender;
        if(!Yii::$app instanceof Yii\console\Application){
            if ($model->hasAttribute('updated_by')) {
                $model->updated_by = Yii::$app->user->id;
                // $model->updated_by = 1;
            }
        }
        
    }
}