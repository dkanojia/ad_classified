<?php
namespace common\behaviors;

use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\db\Expression;

class ARTimestampBehavior extends Behavior
{

    public function events()
    {
        return [
            // ActiveRecord::EVENT_BEFORE_VALIDATE =>'beforeValidate',
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsert',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdate',
        ];
    }

    public function beforeValidate($event)
    {
      $model = $event->sender;
      if($model->isNewRecord)
        $this->beforeInsert($event);
      else
        $this->beforeUpdate($event);
    }

    public function beforeInsert($event)
    {
      $model = $event->sender;
        if ($model->hasAttribute('created_at') && is_null($model->created_at)) {
            $model->created_at = time();
        }
        if ($model->hasAttribute('updated_at')) {
            $model->updated_at = time();
        }
    }

    public function beforeUpdate($event)
    {
        $model = $event->sender;
        if ($model->hasAttribute('updated_at')) {
            $model->updated_at = time();
        }
    }
}