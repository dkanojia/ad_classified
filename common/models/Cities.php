<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cities".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $alias
 * @property int $state_id
 * @property int $country_id
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'state_id', 'country_id'], 'integer'],
            [['name', 'alias', 'state_id'], 'required'],
            [['alias'], 'string'],
            [['name'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'name' => 'Name',
            'alias' => 'Alias',
            'state_id' => 'State ID',
            'country_id' => 'Country ID',
        ];
    }

    public static function getCities($data){
        
        $query = "select cities.id, cities.name, cities.state_id from cities WHERE cities.country_id = 101 ";
        
        if(isset($data) && $data['stateId']) $query = "select cities.id, cities.name, cities.state_id from cities WHERE cities.country_id = 101 AND cities.state_id = ".$data['stateId'];

        // $query->andWhere(['IS NOT', 'id', NULL]);
        // $query->orderBy('id Desc');
        $cities  =  Yii::$app->db->createCommand($query)->queryAll();
        return $cities;
    }

    public static function getCitiesFilteredByName($data){        
        $query = "select cities.id, cities.name, cities.state_id from cities WHERE cities.country_id = 101 ";
        
        if(isset($data) && $data['searchTerm']) $query = "select cities.id, cities.name, cities.state_id, cities.country_id from cities WHERE cities.country_id = 101 AND cities.name LIKE '".$data['searchTerm']."%' ";

        // $query->andWhere(['IS NOT', 'id', NULL]);
        // $query->orderBy('id Desc');
        $cities  =  Yii::$app->db->createCommand($query)->queryAll();
        return $cities;
    }

    public static function getCitiesFilteredByID($data){        
        $query = "select cities.id, cities.name, cities.state_id from cities WHERE cities.country_id = 101 ";
        
        if(isset($data) && $data['cityId']) $query = "select cities.id, cities.name, cities.state_id, cities.country_id from cities WHERE cities.country_id = 101 AND cities.id = ".$data['cityId'];

        // $query->andWhere(['IS NOT', 'id', NULL]);
        // $query->orderBy('id Desc');
        $cities  =  Yii::$app->db->createCommand($query)->queryAll();
        return $cities;
    }
}
