<?php

namespace common\models;

use Yii;
use common\models\AdDetail;
use common\models\AdManagment;

/**
 * This is the model class for table "website_users".
 *
 * @property int $id
 * @property string $user_name
 * @property string $user_email
 * @property string $username
 * @property string $user_password
 * @property int $username_change_count
 * @property string $user_gender
 * @property int $user_phone
 * @property int $user_city_id
 * @property int $user_state_id
 * @property int $user_pincode
 * @property string $user_lat_long
 * @property int $user_country_id
 * @property string $user_dob
 * @property string $user_profile_img
 * @property string $user_created_on
 * @property string $user_last_active
 * @property int $user_blocked
 * @property int $user_deactivated
 * @property int $user_total_ads
 * @property int $user_total_repeated_ads
 * @property int $user_total_offensive_ads
 * @property int $user_total_classified_ads
 * @property int $total_successful_rentals
 * @property int $total_successful_leases
 * @property int $total_blocked_ads
 * @property int $total_warnings
 * @property int $total_requests_made
 * @property int $total_requests_got
 * @property string $status
 * @property string $access_token
 */
class WebsiteUsers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'website_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_name', 'user_email', 'user_password', 'user_gender', 'status'], 'required'],
            [['username_change_count', 'user_phone', 'user_city_id', 'user_state_id', 'user_pincode', 'user_country_id', 'user_blocked', 'user_deactivated', 'user_total_ads', 'user_total_repeated_ads', 'user_total_offensive_ads', 'user_total_classified_ads', 'total_successful_rentals', 'total_successful_leases', 'total_blocked_ads', 'total_warnings', 'total_requests_made', 'total_requests_got'], 'integer'],
            [['user_gender', 'user_profile_img', 'status'], 'string'],
            [['user_dob', 'user_created_on', 'user_last_active'], 'safe'],
            [['user_name', 'user_email', 'user_password', 'access_token'], 'string', 'max' => 255],
            [['username'], 'string', 'max' => 100],
            [['user_lat_long'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_name' => 'User Name',
            'user_email' => 'User Email',
            'username' => 'Username',
            'user_password' => 'User Password',
            'username_change_count' => 'Username Change Count',
            'user_gender' => 'User Gender',
            'user_phone' => 'User Phone',
            'user_city_id' => 'User City ID',
            'user_state_id' => 'User State ID',
            'user_pincode' => 'User Pincode',
            'user_lat_long' => 'User Lat Long',
            'user_country_id' => 'User Country ID',
            'user_dob' => 'User Dob',
            'user_profile_img' => 'User Profile Img',
            'user_created_on' => 'User Created On',
            'user_last_active' => 'User Last Active',
            'user_blocked' => 'User Blocked',
            'user_deactivated' => 'User Deactivated',
            'user_total_ads' => 'User Total Ads',
            'user_total_repeated_ads' => 'User Total Repeated Ads',
            'user_total_offensive_ads' => 'User Total Offensive Ads',
            'user_total_classified_ads' => 'User Total Classified Ads',
            'total_successful_rentals' => 'Total Successful Rentals',
            'total_successful_leases' => 'Total Successful Leases',
            'total_blocked_ads' => 'Total Blocked Ads',
            'total_warnings' => 'Total Warnings',
            'total_requests_made' => 'Total Requests Made',
            'total_requests_got' => 'Total Requests Got',
            'status' => 'Status',
            'access_token' => 'Access Token',
        ];
    }

    public static function getList()
    {
        $query = "select website_users.id, website_users.user_name, website_users.user_gender, cities.name as city, states.name as state, countries.name as country, website_users.user_dob, website_users.user_phone, website_users.user_created_on, website_users.user_last_active, website_users.user_blocked, website_users.user_deactivated, website_users.user_total_ads, website_users.total_requests_made, website_users.total_warnings, website_users.user_email from website_users
            LEFT JOIN cities ON website_users.user_city_id = cities.id
            LEFT JOIN states ON website_users.user_state_id = states.id
            LEFT JOIN countries ON website_users.user_country_id = countries.id
            ORDER BY website_users.user_name
            ";
        // $query->andWhere(['IS NOT', 'id', NULL]);
        // $query->orderBy('id Desc');
        $users  =  Yii::$app->db->createCommand($query)->queryAll();
        return $users;
    }

    public static function verifyApis($access_token ,$required = true){
        if($access_token){
            if(empty($access_token) && $required)
                return ["Ack" => "Error", 'code' => 401, 'Message' =>  'Invalid request due to access-token'];
            $record_id = self::find()->select('id')->where(['access_token' => $access_token,'status' => 'Y'])->one();
            if($record_id){
                return ["Ack" => "Error", 'code' => 200, 'Message' => 'Authorization accepted'];
            }else{
                return ["Ack" => "Error", 'code' => 401, 'Message' =>  'Invalid request due to access-token'];
            }
        } else{            
            return ["Ack" => "Error", 'code' => 401, 'Message' =>  'Invalid request access token is not available'];
        }        
    }

    public static function getUserList($data){
        if($data){
            if(empty($data['email']) && $required)
                return ["Ack" => "Error", 'code' => 401, 'Message' =>  'EMAIL_MISSING'];
            if(empty($data['password']) && $required)
                return ["Ack" => "Error", 'code' => 401, 'Message' =>  'PASSWORD_MISSING'];
            $password = hash('gost',$data['password']);
            $record_id = self::find()->select('*')->where(['user_email' => $data['email'],'user_password' => $password])->one();
            if($record_id){
                if($record_id['user_blocked'] == 0){
                    if($record_id['user_deactivated'] == 0){
                        $access_token = bin2hex(random_bytes(10));  
                        WebsiteUsers::updateAll(
                            array('status' => 'Y','access_token' => $access_token,'user_last_active' => date('Y-m-d H:i:s')),
                            'user_email = "'.$data['email'].'" AND user_password = "'.$password.'" AND id = "'.$record_id['id'].'"');
                        return ['code' => 200, 'data' => ['id' => $record_id['id'],'access_token' => $access_token,'user_name' => $record_id['user_name'],'user_gender' => $record_id['user_gender'],'user_city_id' => $record_id['user_city_id'],'user_state_id' => $record_id['user_state_id'],'user_dob' => $record_id['user_dob'],'user_phone' => $record_id['user_phone'],'user_email'=>$record_id['user_email'],'user_created_on' => $record_id['user_created_on'],'user_last_active' => $record_id['user_last_active'],'user_pincode' => $record_id['user_pincode']]];
                    }
                    else{
                        return ["success" => "false", "Ack" => "Error", 'code' => 401, 'Message' =>  'USER_DEACTIVATED'];    
                    }
                }
                else{
                    return ["success" => "false", "Ack" => "Error", 'code' => 401, 'Message' =>  'USER_BLOCKED'];    
                }
            }else{
                return ["success" => "false", "Ack" => "Error", 'code' => 401, 'Message' =>  'INVALID_USERNAME_PASSWORD'];
            }
        } else{
            return ["success" => "false", "Ack" => "Error", 'code' => 401, 'Message' =>  'INVALID_REQUEST'];
        }   
    }

    public static function signUp($data){
        if($data){
            if(empty($data['email']) && $required)
                return ["success" => "false", "Ack" => "Error", 'code' => 401, 'Message' =>  'EMAIL_MISSING'];
            if(empty($data['password']) && $required)
                return ["success" => "false", "Ack" => "Error", 'code' => 401, 'Message' =>  'PASSWORD_MISSING'];
            $password = hash('gost',$data['password']);
            $result = self::findOne(['user_email' => $data['email'],'user_password' => $password]);
            if($result){
                return ["success" => "false", 'code' => 201, 'access_token' =>  $result['access_token'], 'message' => 'USER_EXISTS'];
            }else{
                $access_token = bin2hex(random_bytes(10));  
                $model = new WebsiteUsers();
                $model->user_email = $data['email'];
                $model->user_gender = $data['gender'];
                $model->user_phone = $data['user_phone'];
                $model->user_name = $data['user_name'];
                $model->user_password = $password;
                $model->user_created_on = date('Y-m-d H:i:s');
                $model->status = 'Y';
                $model->access_token = $access_token;
                $saveDetails = $model->save();
                if(!$saveDetails){
                    print_r($model->getErrors());die;
                }else{
                    $result = self::findOne(['user_email' => $data['email'],'user_password' => $password]);
                    if($result){  
                        return ['code' => 200, 'access_token' =>  $access_token, 'data' => $result];
                    } else {                        
                        return ['code' => 200, 'access_token' =>  $access_token, 'data' => $saveDetails];
                    }
                }
            }
        } else{            
            return ["success" => "false", "Ack" => "Error", 'code' => 401, 'Message' =>  'INVALID_REQUEST'];
        }
    }

    public static function changePasswordForFrontendController($data){
        $web_users = WebsiteUsers::find()
            ->where(
                    [
                        'id' => $data['user_id']
                    ]
                )
            ->one();

        if(!empty($web_users) || ($web_users != null)){
            
            // $existing_password = hash('gost',$web_users->user_password);
            $old_password = hash('gost',$data['old_password']);
            $new_password = hash('gost',$data['new_password']);

            if($web_users->user_password != $old_password)
                return ["Ack" => "Error", 'code' => 401, 'Message' =>  'Old password doesn\'t match'];

            WebsiteUsers::updateAll(
                        array('user_password' => $new_password),  'id = '.$data['user_id'].' AND user_password = \''.$old_password.'\'' 
                        );

        }
        return $data;
    }

    public static function fetchUserDetails($data){
        $web_users = WebsiteUsers::find()
            ->where(
                    [
                        'id' => $data['user_id']
                    ]
                )
            ->one();

            return $web_users;
    }

    public static function accountProfileUpdate($data){
        $web_users = WebsiteUsers::find()
            ->where(
                    [
                        'id' => $data['user_id']
                    ]
                )
            ->one();

        if(!empty($web_users) || ($web_users != null)){
            WebsiteUsers::updateAll(
                array(
                    'user_name' => isset($data['user_name']) ? $data['user_name'] : $web_users->user_name,
                    // 'user_email' => isset($data['user_email']) ? $data['user_email'] : $web_users->user_email,
                    // 'username' => isset($data['username']) ? $data['username'] : $web_users->username,
                    'user_gender' => isset($data['user_gender']) ? $data['user_gender'] : $web_users->user_gender,
                    'user_phone' => isset($data['user_phone']) ? $data['user_phone'] : $web_users->user_phone,
                    'user_city_id' => isset($data['user_city_id']) ? $data['user_city_id'] : $web_users->user_city_id,
                    'user_state_id' => isset($data['user_state_id']) ? $data['user_state_id'] : $web_users->user_state_id,
                    'user_pincode' => isset($data['user_pincode']) ? $data['user_pincode'] : $web_users->user_pincode,
                    'user_dob' => isset($data['user_dob']) ? $data['user_dob'] : $web_users->user_dob,
                    ),  
                    'id = '.$data['user_id']
                );
        }

        return $data;
    }

    public static function accountDeactive($data){
        $web_users = WebsiteUsers::find()
            ->where(
                    [
                        'id' => $data['user_id']
                    ]
                )
            ->one();

        if(!empty($web_users) || ($web_users != null)){
            $deactivated = WebsiteUsers::updateAll(
                            array('user_deactivated' => 1),  'id = '.$data['user_id'] 
                        );  
            
            if($deactivated == 1){

                WebsiteUsers::updateAll(
                            array('status' => 'N','access_token' => NULL,'user_last_active' => date('Y-m-d H:i:s')),  'id = '.$data['user_id']
                            );

                // Ad Deactive
                $ad_deactivated = AdDetail::updateAll(
                                array(
                                  'ad_deactivated' => 1
                                  ),
                                'user_id = '.$data['user_id'] 
                            );

                if($ad_deactivated == 1){
                    
                    $ad_list = AdDetail::getDeactivatedAdList($data);
                    
                    foreach ($ad_list as $ad_key => $ad_value) {
                        AdManagment::updateAll(
                                    array('is_ad_deactivated' => 1),  
                                        'ad_id = '.$ad_value['id']
                                    );
                    }


                }
            }

        }

        return $data;
    }

    public static function accountLogout($data){
        $web_users = WebsiteUsers::find()
            ->where(
                    [
                        'id' => $data['user_id']
                    ]
                )
            ->one();

        if(!empty($web_users) || ($web_users != null)){
            WebsiteUsers::updateAll(
                        array('status' => 'N','access_token' => NULL,'user_last_active' => date('Y-m-d H:i:s')),  'id = '.$data['user_id']
                        );
        }

        return $data;
    }

    /*New Insertion at 27-3-2019*/
    public static function changePassword($data){
        if($data){
            if(empty($data['email']) && $required)
                return ["Ack" => "Error", 'code' => 401, 'Message' =>  'Invalid request due to email'];
            if(empty($data['password']) && $required)
                return ["Ack" => "Error", 'code' => 401, 'Message' =>  'Invalid request due to password'];
            $password = hash('gost',$data['password']);
            $new_password = hash('gost',$data['new_password']);
            $record_id = self::find()->where(['user_email' => $data['email'],'user_password' => $password,'status' => 'Y'])->one();
            if($record_id){
                $record_id->user_password = $new_password;
                if(!$record_id->save()){
                    print_r($record_id->getErrors());die;
                }
                return ["Ack" => "Error", 'code' => 200, 'Message' =>  'Reset password successfully'];  
            }else{
                return ["Ack" => "Error", 'code' => 401, 'Message' =>  'Invalid password'];
            }
        } else{            
            return ["Ack" => "Error", 'code' => 401, 'Message' =>  'Invalid request'];
        }   
    }

    public static function forgetPassword($data){
        if($data){
            if(empty($data['email']) && $required){
                return ["success" => "false", "Ack" => "Error", 'code' => 401, 'message' =>  'EMAIL_REQUIRED'];
            }
            $password = bin2hex(random_bytes(7));
            $new_password = hash('gost',$password);
            $record_id = self::find()->where(['user_email' => $data['email']])->one();
            if($record_id){
                $record_id->user_password = $new_password;
                if(!$record_id->save()){
                    print_r($record_id->getErrors());die;
                }else{
                    Yii::$app->mailer->compose()
                     ->setFrom('admin@worldonrent.com')
                     ->setTo($data['email'])
                     ->setSubject('WorldOnRent - Password Reset Request')
                     ->setHtmlBody('<br /> Hi, we have received password reset request. Please login with your new password.<br /> New Password: '.$password.'<br /><br />Please consider resetting your password after logging in.<br /> Thanks')
                     ->send();
                    return ['code' => 200, 'message' =>  'PASSWORD_SENT' , 'new_password' => $password];  
                }
            }else{
                return ["success" => "false", "Ack" => "Error", 'code' => 401, 'message' =>  'EMAIL_DOES_NOT_EXIST'];
            }
        } else{            
            return ["success" => "false", "Ack" => "Error", 'code' => 401, 'message' =>  'INVALID_REQUEST'];
        }   
    }

    public static function userAccountBlockUnblock($data){
        $web_users = WebsiteUsers::find()
            ->where(
                    [
                        'id' => $data['website_user_id']
                    ]
                )
            ->one();

        if(!empty($web_users) || ($web_users != null)){
            $user_blocked_unblock = WebsiteUsers::updateAll(
                            array('user_blocked' => $data['new_status']),  'id = '.$data['website_user_id'] 
                        );  
            
            if($user_blocked_unblock == 1){

                AdDetail::updateAll(
                            array( 'ad_blocked' => $data['new_status'], 'ad_last_modified' => date('Y-m-d H:i:s') ),  ' user_id = '.$data['website_user_id'] 
                            );            
            }

        }
        return $data;
    }

    // public static function actionWebsiteUserUnBlock($data){
    //     $web_users = WebsiteUsers::find()
    //         ->where(
    //                 [
    //                     'id' => $data['website_user_id']
    //                 ]
    //             )
    //         ->one();

    //     if(!empty($web_users) || ($web_users != null)){
    //         $user_un_blocked = WebsiteUsers::updateAll(
    //                         array('user_blocked' => 0),  'id = '.$data['website_user_id'] 
    //                     );  
            
    //         if($user_un_blocked == 1){

    //             AdDetail::updateAll(
    //                         array('ad_blocked' => 0, 'ad_last_modified' => date('Y-m-d H:i:s') ),  ' user_id = '.$data['website_user_id']
    //                         );            
    //         }

    //     }

    //     return $data;
    // }
}
