<?php

namespace common\models;

use Yii;
use common\models\Notifications;
use common\models\AdDetail;
use common\models\WebsiteUsers;

/**
 * This is the model class for table "ad_managment".
 *
 * @property int $id
 * @property int $rentor_id
 * @property int $rentee_id
 * @property int $ad_id
 * @property string $rentee_message
 * @property string $request_status
 * @property string $date_requested
 * @property string $date_resolved
 * @property int $is_ad_blocked
 * @property int $is_ad_deleted
 */
class AdManagment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_managment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rentor_id', 'rentee_id', 'ad_id', 'request_status', 'date_requested'], 'required'],
            [['rentor_id', 'rentee_id', 'ad_id', 'is_ad_blocked', 'is_ad_deleted','is_ad_deactivated'], 'integer'],
            [['rentee_message', 'request_status'], 'string'],
            [['date_requested', 'date_resolved'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rentor_id' => 'Rentor ID',
            'rentee_id' => 'Rentee ID',
            'ad_id' => 'Ad ID',
            'rentee_message' => 'Rentee Message',
            'request_status' => 'Request Status',
            'date_requested' => 'Date Requested',
            'date_resolved' => 'Date Resolved',
            'is_ad_blocked' => 'Is Ad Blocked',
            'is_ad_deleted' => 'Is Ad Deleted',
        ];
    }

    public static function putAdRequest($data){

        if($data['rentor_id'] == $data['rentee_id']){
            $b = $data['can_not_be_same_rentor_or_rentee_id'];
        }

        $ad_managment = AdManagment::find()
            ->where(
                [
                    'rentor_id' => $data['rentor_id'],
                    'rentee_id' => $data['rentee_id'],
                    'ad_id' => $data['ad_id'],
                    'request_status' => 'in_queue'
                ]
                )
            ->one();

        if(!empty($ad_managment) || ($ad_managment != null)){
            $in_queue = 'in_queue';
            AdManagment::updateAll(
                        array('rentee_message' => $data['rentee_message']),  'rentor_id = '.$data['rentor_id'].' AND rentee_id = '.$data['rentee_id'].' AND ad_id = '.$data['ad_id'].' AND request_status = \'in_queue\''
                        );   

        }else{
            
            $model = new AdManagment();
            $model->rentor_id = $data['rentor_id'];
            $model->rentee_id = $data['rentee_id'];
            $model->ad_id = $data['ad_id'];
            $model->rentee_message = $data['rentee_message'];
            $model->request_status = "in_queue";
            $model->date_requested = date("Y-m-d H:i:s");
            $model->is_ad_blocked = 0;
            $model->is_ad_deleted = 0;
            $model->save();   
        
            /*Total Requests*/
            $is_exist = AdDetail::find()
                        ->where(
                            [
                                'id' => $data['ad_id']
                            ]
                            )
                        ->one();

            if(!empty($is_exist) || ($is_exist != null)){
                AdDetail::updateAll(
                                array(
                                  'total_requests' => ($is_exist->total_requests + 1 )
                                  ),
                                'id = '.$data['ad_id'] 
                            );
            }

            /*Website users*/
            $is_rentee_exist = WebsiteUsers::find()
                        ->where(
                            [
                                'id' => $data['rentee_id']
                            ]
                            )
                        ->one();

            if(!empty($is_rentee_exist) || ($is_rentee_exist != null)){
                WebsiteUsers::updateAll(
                                array(
                                  'total_requests_made' => ($is_rentee_exist->total_requests_made + 1)
                                  ),
                                'id = '.$data['rentee_id'] 
                            );
            }

            /*Website users*/
            $is_rentor_exist = WebsiteUsers::find()
                        ->where(
                            [
                                'id' => $data['rentor_id']
                            ]
                            )
                        ->one();

            if(!empty($is_rentor_exist) || ($is_rentor_exist != null)){
                WebsiteUsers::updateAll(
                                array(
                                  'total_requests_got' => ($is_rentor_exist->total_requests_got + 1)
                                  ),
                                'id = '.$data['rentor_id'] 
                            );
            }

        }

        /*Notifications*/
        $notify_data = [
                    'type' => 'request_received',
                    'content' => $data['notificationMessageString'],
                    'payload' => json_encode($data['payload']),
                    'user_id' => $data['rentor_id']
                    ];
        $not_mdl = new Notifications();
        $not_mdl->saveNotifications($notify_data);

        return $data;
    }

    /*Update Request Status For Accept Or Declined*/
    public static function updateAdRequestStatus($data){
        $ad_managment = AdManagment::find()
            ->where(
                [
                    'rentor_id' => $data['rentor_id'],
                    'rentee_id' => $data['rentee_id'],
                    'ad_id' => $data['ad_id'],
                    'id' => $data['ad_managment_id'],
                    'request_status' => 'in_queue'
                ]
                )
            ->one();

        if(!empty($ad_managment) || ($ad_managment != null)){
            $in_queue = 'in_queue';
            $ad_updated = AdManagment::updateAll(
                        array('request_status' => $data['request_status'], 'date_resolved' => date('Y-m-d H:i:s')),  'rentor_id = '.$data['rentor_id'].' AND rentee_id = '.$data['rentee_id'].' AND ad_id = '.$data['ad_id'].' AND request_status = \'in_queue\' AND id = '.$data['ad_managment_id']
                        );   

        }
        
        if(($ad_updated == 1) && ($data['request_status'] == 'accepted') ){

            /*Website users*/
            $is_rentee_exist = WebsiteUsers::find()
                        ->where(
                            [
                                'id' => $data['rentee_id']
                            ]
                            )
                        ->one();

            if(!empty($is_rentee_exist) || ($is_rentee_exist != null)){
                WebsiteUsers::updateAll(
                                array(
                                  'total_successful_rentals' => ($is_rentee_exist->total_successful_rentals + 1)
                                  ),
                                'id = '.$data['rentee_id'] 
                            );
            }

            /*Website users*/
            $is_rentor_exist = WebsiteUsers::find()
                        ->where(
                            [
                                'id' => $data['rentor_id']
                            ]
                            )
                        ->one();

            if(!empty($is_rentor_exist) || ($is_rentor_exist != null)){
                WebsiteUsers::updateAll(
                                array(
                                  'total_successful_leases' => ($is_rentor_exist->total_successful_leases + 1)
                                  ),
                                'id = '.$data['rentor_id'] 
                            );
            }
            
        }


        /*Notifications*/
        $notify_data = [
            'type' => $data['n_request_status'],
            'content' => $data['notificationMessageString'],
            'payload' => json_encode($data['payload']),
            'user_id' => $data['rentee_id']
        ];
        $not_mdl = new Notifications();
        $not_mdl->saveNotifications($notify_data);

        return $data;
    }
}
