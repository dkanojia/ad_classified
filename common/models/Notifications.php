<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "notifications".
 *
 * @property int $id
 * @property string $n_date
 * @property string $n_type
 * @property string $n_mark_as_read
 * @property int $n_deleted
 * @property string $n_content
 * @property string $n_payload
 * @property int $n_user_id who will receive notification?
 */
class Notifications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notifications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['n_date', 'n_type', 'n_content', 'n_payload', 'n_user_id'], 'required'],
            [['n_date', 'n_mark_as_read'], 'safe'],
            [['n_type', 'n_content', 'n_payload'], 'string'],
            [['n_deleted', 'n_user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'n_date' => 'N Date',
            'n_type' => 'N Type',
            'n_mark_as_read' => 'N Mark As Read',
            'n_deleted' => 'N Deleted',
            'n_content' => 'N Content',
            'n_payload' => 'N Payload',
            'n_user_id' => 'N User ID',
        ];
    }

    public static function saveNotifications($data){
        $model = new Notifications();
        
        $model->n_date = date('Y-m-d H:i:s');
        $model->n_type = $data['type'];
        $model->n_content = $data['content'];
        $model->n_payload = $data['payload'];
        $model->n_user_id = $data['user_id'];
        
        $model->save();
        
        return true;
    }

    public static function getNotifications($data){
        
        if(!isset($data)) return;

        isset($data['all_notification_count']) ? ($limit = $data['all_notification_count']) : ($limit = 30); 
        
        $new = self::find()
                // ->select('id','n_date','n_type','n_content','n_payload')
                ->where(['n_mark_as_read' => NULL, 'n_deleted' => 0, 'n_user_id' => $data['user_id']])
                ->asArray()
                ->orderBy(['id'=>SORT_DESC])
                ->all();


        $all = self::find()
                // ->select('id','n_date','n_type','n_content','n_payload')
                ->where(['n_deleted' => 0, 'n_user_id' => $data['user_id']])
                ->asArray()
                ->limit($limit)
                ->orderBy(['id'=>SORT_DESC])
                ->all();
        
        $get_count = self::find()
                ->select(['COUNT(*) AS cnt'])
                ->where(['n_mark_as_read' => NULL, 'n_deleted' => 0, 'n_user_id' => $data['user_id']])
                ->groupBy(['n_user_id'])
                ->all()
                ;

        return $final_data = [
                'new' => $new,
                'all' => $all,
                'count' => empty($get_count) ? 0 : $get_count
        ];
    }

    public static function markReadNotification($data){
        return Notifications::updateAll(
                        array(
                            'n_mark_as_read' => date('Y-m-d H:i:s')
                            ),  
                            'n_user_id = '.$data['user_id'] 
                        );
    }
}
