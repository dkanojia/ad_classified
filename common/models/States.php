<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "states".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property int $country_id
 */
class States extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'states';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'country_id'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'name' => 'Name',
            'country_id' => 'Country ID',
        ];
    }

    public static function getStates(){
        $query = "select states.id, states.name from states WHERE states.country_id = 101 ";
        $states  =  Yii::$app->db->createCommand($query)->queryAll();
        return $states;
    }
}
