<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $sortname
 * @property string $name
 * @property int $status
 * @property string $geo_location
 * @property string $country_code
 */
class Countries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sortname', 'name'], 'required'],
            [['id', 'parent_id', 'status'], 'integer'],
            [['sortname'], 'string', 'max' => 3],
            [['name'], 'string', 'max' => 150],
            [['geo_location'], 'string', 'max' => 50],
            [['country_code'], 'string', 'max' => 10],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'sortname' => 'Sortname',
            'name' => 'Name',
            'status' => 'Status',
            'geo_location' => 'Geo Location',
            'country_code' => 'Country Code',
        ];
    }
}
