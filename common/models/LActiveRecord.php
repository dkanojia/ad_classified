<?php

namespace common\models;

use Yii;
use webvimark\modules\UserManagement\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class LActiveRecord extends \yii\db\ActiveRecord
{
    public $uploadsPath;
    public static $_uploadsPath;
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_DELETED = 2;
    const STATUS_COMPLETED = 3;
    const SCENARIO_SUPERADMIN = 'superadmin';
  
    public function init(){
        parent::init();
        if(!is_a(Yii::$app,'yii\console\Application')){
            $url = Url::base(true);
            if(strpos($url,'backend')!==false){
                self::$_uploadsPath =  $this->uploadsPath =$url.'/../uploads';
            }else {
                self::$_uploadsPath =  $this->uploadsPath =$url.'/../../uploads';
            }
        
        }

    }

  public function behaviors()
  {
    parent::behaviors();
    return [
      [
        'class' => \common\behaviors\ARTimestampBehavior::className(),
      ],
      'blame'=>[
        'class' => \common\behaviors\ARBlameableBehavior::className(),
      ],
    ];
  }

  public static function attributeLabel()
  {
    return [
        'id' => 'ID',
        'name' => 'Name',
        'country_id' => Yii::t('app', 'Country'),
        'state_id' => Yii::t('app', 'State'),
        'city_id' => Yii::t('app', 'City'),
        'remark' => Yii::t('app', 'Remark'),
        'description' => Yii::t('app', 'Description'),
        'created_by' => Yii::t('app', 'Added By'),
        'updated_by' => Yii::t('app', 'Last Modified By'),
        'created_at' => Yii::t('app', 'Added At'),
        'updated_at' => Yii::t('app', 'Last Modified At'),
        'status' => Yii::t('app', 'Status'),
        'statusName' => Yii::t('app', 'Status'),
        'createdByName' => Yii::t('app', 'Added By'),
        'updatedByName' => Yii::t('app', 'Last Modified By'),
        'createdAtDate' => Yii::t('app', 'Added At'),
        'updatedAtDate' => Yii::t('app', 'Last Modified At'),
        'event_id' => Yii::t('app', 'Event'),
        'session_id' => Yii::t('app', 'Session'),
        'user_id' => 'User',
        'feedback_id' => 'Feedback',
        'track_id' => 'Track'
    ];
  }

  public static function validateMobile($mobile)
  {
    return [$mobile, 'match', 'pattern' => '/^[1-9]{1}[0-9]{9}$/'];
  }

  public static function validateLandline($landline)
  {
    return [$landline, 'match', 'pattern' => '/^[0]{0,1}[0-9]{10}$/'];
  }

  public static function validateFax($fax)
  {
    return [$fax, 'match', 'pattern' => '/^\\+[0-9]{1,3}-[0-9]{3}-[0-9]{7}$/'];
  }

    public function setDefault()
    {
        $this->status = 1;
    }

   /**
     * get all items with active status
     * @return [type] [description]
     */
    public static function getActiveItems($fields = null, $return_type = 'array', $where = null, $order_by = 'name ASC')
    {
        if(is_null($fields))
            $fields = ['id', 'name'];
        $query = static::find()->select($fields)->where(['status' => static::STATUS_ACTIVE]);
         if( $query->modelClass == 'events\models\Events'){
            $user_events = isset(Yii::$app->request->cookies)?Yii::$app->request->cookies->getValue('event_id'):'';
             if(!empty($user_events))
            {
                //$evt = explode(',', $user_events);
                $query->andWhere(['id' => $user_events]);
                
            }   
            
        } else if( $query->modelClass == 'events\models\Session'){
            $user_events = isset(Yii::$app->request->cookies)?Yii::$app->request->cookies->getValue('event_id'):'';
             if(!empty($user_events))
            {
                //$evt = explode(',', $user_events);
                $query->andWhere(['event_id' => $user_events]);
                
            }   
            
        }
        
        if($where){
          $query->andFilterWhere($where);
        }
        
        $query->orderBy($order_by);

        if($return_type == 'array') {
            $query = $query->asArray();
        }
        else if($return_type == 'query') {
            return $query;
        }
        return $query->all();
    }
   /**
     * get all items with active status
     * @return [type] [description]
     */
    public static function getItems($fields = null, $return_type = 'array', $where = null, $order_by = 'name ASC')
    {
        if(is_null($fields))
            $fields = ['id', 'name'];
        $query = self::find()->select($fields)->where(['!=', 'status', self::STATUS_DELETED]);
        if( $query->modelClass == 'events\models\Events'){
            $user_events = isset(Yii::$app->user->identity)?Yii::$app->user->identity->user_events:[];
        
            if(!empty($user_events))
            {
                $evt = explode(',', $user_events);
                $query->andWhere(['in','id',$evt]);
                
            }   
            
        }else if( $query->modelClass == 'events\models\Session'){
            $user_events = isset(Yii::$app->user->identity)?Yii::$app->user->identity->user_events:[];
        
            if(!empty($user_events))
            {
                $evt = explode(',', $user_events);
                $query->andWhere(['in','event_id',$evt]);
                
            }   
            
        }
        
        if($where){
          $query->andFilterWhere($where);
        }
        
        $query->orderBy($order_by);

        if($return_type == 'array') {
            $query = $query->asArray();
        }
        else if($return_type == 'query') {
            return $query;
        }
        return $query->all();
    }

  public function setAsEnabled()
  {
    $this->status = self::STATUS_ACTIVE;
  }

  public function setAsDisabled()
  {
    $this->status = self::STATUS_INACTIVE;
  }

  public function setAsDeleted()
  {
    $this->status = self::STATUS_DELETED;
  }

  public function getIsEnabled()
  {
    return $this->status == self::STATUS_ACTIVE;
  }

  public function getStatusName()
  {
    if(isset($this->status))
      return self::getAllStatus($this->status);
    else
      return '';
  }

  public function getCreatedByName()
  {
    if(isset($this->created_by) && $this->created_by>0) {
      // $user = User::find()->where(['id' => $this->created_by])->andWhere('blocked_at is NULL')->one();
      $user = User::find()->where(['id' => $this->created_by])->one();
      if(!empty($user)){
        return $user->username;
        // return $user->first_name.' '.$user->last_name;
      }
      else
        return 'User Not Available';
    }
    else {
      return 'User was not logged in';
    }
  }

  public function getUpdatedByName()
  {
    if(isset($this->created_by) && $this->created_by>0) {
      // $user = User::find()->where(['id' => $this->created_by])->andWhere('blocked_at is NULL')->one();
      $user = User::find()->where(['id' => $this->created_by])->one();
      if(!empty($user)){
        return $user->username;
      }
      else
        return 'User Not Available';
    }
    else {
      return 'User was not logged in';
    }
  }

  public function getCreatedAt()
  {
    if(isset($this->created_at))
      return date('M d, Y h:i A' , $this->created_at);
    else
      return '';
  }

  public function getUpdatedAt()
  {
    if(isset($this->updated_at))
      return date('M d, Y h:i A' , $this->updated_at);
    else
      return '';
  }

  public function getCreatedAtDate()
  {
    if(isset($this->created_at))
      return date('M d, Y' , $this->created_at);
    else
      return '';
  }

  public function getCreatedAtDateTime()
  {
    if(isset($this->created_at))
      return date('M d, Y h:i a' , $this->created_at);
    else
      return '';
  }

  public function getUpdatedAtDate()
  {
    if(isset($this->updated_at))
      return date('M d, Y' , $this->updated_at);
    else
      return '';
  }

  public function setDefaultPriority()
  {
    if($this->hasAttribute('priority'))
      $this->priority = 4;
  }

  public static function getStatus($code = null)
  {
    $_items = [
      self::STATUS_ACTIVE => Yii::t('app', 'Active'),
      self::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
    ];

    if(isset($code)){
      return isset($_items[$code])?$_items[$code]:null;
    }
    else{
      return $_items;
    }
  }

  public static function getBooleanFilter($code = null)
  {
    $_items = [
      '1' => Yii::t('app', 'Yes'),
      '0' => Yii::t('app', 'No'),
    ];

    if(isset($code)){
      return isset($_items[$code])?$_items[$code]:null;
    }
    else{
      return $_items;
    }
  }
  public static function getPollStatus($code = null)
  {
    $_items = [
      self::STATUS_ACTIVE => Yii::t('app', 'Active'),
      self::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
      self::STATUS_COMPLETED => Yii::t('app', 'Completed'),
    ];

    if(isset($code)){
      return isset($_items[$code])?$_items[$code]:null;
    }
    else{
      return $_items;
    }
  }
  public static function getAllStatus($code = null)
  {
    $_items = [
      self::STATUS_ACTIVE => Yii::t('app', 'Active'),
      self::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
      self::STATUS_DELETED => Yii::t('app', 'Deleted'),
      self::STATUS_COMPLETED => Yii::t('app', 'Completed'),
    ];

    if(isset($code)){
      return isset($_items[$code])?$_items[$code]:null;
    }
    else{
      unset($_items[self::STATUS_DELETED]);
      return $_items;
    }
  }

  public static function getActiveItemsForDdl($model, $where=null, $key='id', $value='name', $order_by = 'name', $format_by = '', $fields='')
  {
    if(!$fields)
      $fields = [$key,$value];
    if(!$format_by)
      $format_by = $value;
    /*if($model == 'events\models\Events'){
        $user_events = Yii::$app->user->identity->user_events;
        
        if(!empty($user_events))
        {
            $evt = explode(',', $user_events);
            if(empty($where)){
                $where = ['in','id',$evt];
            }
            else {
                $where .= ['in','id',$evt];
            }
        }    
    }*/
    return (ArrayHelper::map($model::getActiveItems($fields, '', $where, $order_by), $key, $format_by));
  }

  public static function getItemsForDdl($model, $where=null, $key='id', $value='name', $order_by = 'name')
  {
      
    return (ArrayHelper::map($model::getItems([$key, $value], 'array', $where, $order_by), $key, $value));
  }
  
  public static function createMultiple($modelClass, $multipleModels = [])
   {
       $model    = new $modelClass;
       $formName = $model->formName();
       $post     = Yii::$app->request->post($formName);
       $models   = [];

       if (! empty($multipleModels)) {
           $keys = array_keys(ArrayHelper::map($multipleModels, 'id', 'id'));
           $multipleModels = array_combine($keys, $multipleModels);
       }

       if ($post && is_array($post)) {
           foreach ($post as $i => $item) {
               if (isset($item['id']) && !empty($item['id']) && isset($multipleModels[$item['id']])) {
                   $models[] = $multipleModels[$item['id']];
               } else {
                   $models[] = new $modelClass;
               }
           }
       }

       unset($model, $formName, $post);

       return $models;
   }

   public function deleteMe(){
      $this->status = self::STATUS_DELETED;
      return $this->save(false);
   }

   public function disableMe(){
      $this->status = self::STATUS_INACTIVE;
      return $this->save(false);
   }

   public function enableMe(){
      $this->status = self::STATUS_ACTIVE;
      return $this->save(false);
   }

    public function getFormatDate()
    {
        if(isset($this->date))
          return date('M d, Y' , strtotime($this->date));
        else
          return '';
    }
    public function getFormatDateWithDay()
    {
        if(isset($this->date))
          return date('D, M d' , strtotime($this->date));
        else
          return '';
    }
    public function getFormatStartTime()
    {
        if(isset($this->start_time))
          return date('h:i a' , strtotime($this->start_time));
        else
          return '';
    }

    public function getFormatEndTime()
    {
        if(isset($this->end_time))
          return date('h:i a' , strtotime($this->end_time));
        else
          return '';
    }
    public function getFormatStartTime24()
    {
        if(isset($this->start_time))
          return date('H:i:s' , strtotime($this->start_time));
        else
          return '';
    }

    public function getFormatEndTime24()
    {
        if(isset($this->end_time))
          return date('H:i:s' , strtotime($this->end_time));
        else
          return '';
    }
}
