<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tnc".
 *
 * @property string $web_content
 * @property string $auto_saved_content
 * @property string $last_updated on auto saved
 * @property string $last_saved on manually saved
 */
class Tnc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tnc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['web_content', 'last_updated', 'last_saved'], 'required'],
            [['web_content', 'auto_saved_content'], 'string'],
            [['last_updated', 'last_saved'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'web_content' => 'Web Content',
            'auto_saved_content' => 'Auto Saved Content',
            'last_updated' => 'Last Updated',
            'last_saved' => 'Last Saved',
        ];
    }

    public static function getTnc(){
        return self::find()->select('web_content, last_updated, last_saved')->asArray()->All();
    }

    public static function getAutoSaveTnc(){
        return self::find()->select('auto_saved_content, last_updated, last_saved')->asArray()->All();
    }

    public static function autoSaveTnc($data){
        Tnc::updateAll(array('auto_saved_content' => $data['body'],'last_updated' => $data['last_updated']));
        return $data['body'];
    }

    public static function saveTncForWebsite($data){
        Tnc::updateAll(array('web_content' => $data['body'], 'auto_saved_content' => $data['body'], 'last_saved' => date('Y-m-d H:i:s')));
        return $data['body'];
    }
}
