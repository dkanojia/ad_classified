<?php

namespace common\models;

use common\models\AdDetail;

use Yii;

/**
 * This is the model class for table "article_detail".
 *
 * @property int $id
 * @property string $item_name
 * @property int $item_category_id
 * @property int $item_parent_category_id
 * @property string $item_description
 * @property int $item_country_id
 * @property int $item_state_id
 * @property int $item_city_id
 * @property string $item_renting_period
 * @property string $item_renting_start_date
 * @property string $item_renting_end_date
 * @property int $item_rent_per_hour
 * @property int $item_rent_per_day
 * @property int $item_rent_per_week
 * @property int $item_rent_per_monthly
 * @property int $item_rent_per_year
 * @property int $security_deposit
 * @property int $quantity
 * @property string $item_condition
 * @property string $item_special_instruction
 * @property string $img_src_payload
 */
class ArticleDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_name', 'item_category_id', 'item_parent_category_id', 'item_description', 'item_country_id', 'item_state_id', 'item_city_id'], 'required'],
            [['item_category_id', 'item_parent_category_id', 'item_country_id', 'item_state_id', 'item_city_id', 'item_rent_per_hour', 'item_rent_per_day', 'item_rent_per_week', 'item_rent_per_monthly', 'item_rent_per_year', 'security_deposit', 'quantity'], 'integer'],
            [['item_description', 'item_condition', 'item_special_instruction', 'img_src_payload'], 'string'],
            [['item_renting_start_date', 'item_renting_end_date'], 'safe'],
            [['item_name', 'item_renting_period'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_name' => 'Item Name',
            'item_category_id' => 'Item Category ID',
            'item_parent_category_id' => 'Item Parent Category ID',
            'item_description' => 'Item Description',
            'item_country_id' => 'Item Country ID',
            'item_state_id' => 'Item State ID',
            'item_city_id' => 'Item City ID',
            'item_renting_period' => 'Item Renting Period',
            'item_renting_start_date' => 'Item Renting Start Date',
            'item_renting_end_date' => 'Item Renting End Date',
            'item_rent_per_hour' => 'Item Rent Per Hour',
            'item_rent_per_day' => 'Item Rent Per Day',
            'item_rent_per_week' => 'Item Rent Per Week',
            'item_rent_per_monthly' => 'Item Rent Per Monthly',
            'item_rent_per_year' => 'Item Rent Per Year',
            'security_deposit' => 'Security Deposit',
            'quantity' => 'Quantity',
            'item_condition' => 'Item Condition',
            'item_special_instruction' => 'Item Special Instruction',
            'img_src_payload' => 'Img Src Payload',
        ];
    }

    public static function articleDetailSave($data){
        $model = new ArticleDetail();
        $model->item_name = $data['item_name'];
        $model->item_category_id = $data['item_category_id'];
        $model->item_parent_category_id = isset($data['item_parent_category_id']) ? $data['item_parent_category_id'] : 0;
        $model->item_description = isset($data['item_description']) ? $data['item_description'] : "";
        $model->item_country_id = '101';
        $model->item_city_id = $data['item_city_id'];
        $model->item_state_id = $data['item_state_id'];
        // $model->item_renting_period = null;
        // $model->item_renting_start_date = null;
        // $model->item_renting_end_date = null;

        $model->item_rent_per_hour = null;
        $model->item_rent_per_day = isset($data['item_rent_per_day']) ? $data['item_rent_per_day'] : null;
        $model->item_rent_per_week = isset($data['item_rent_per_week']) ? $data['item_rent_per_week'] : null;
        $model->item_rent_per_monthly = isset($data['item_rent_per_monthly']) ? $data['item_rent_per_monthly'] : null;
        $model->item_rent_per_year = isset($data['item_rent_per_year']) ? $data['item_rent_per_year'] : null;
        $model->security_deposit = isset($data['security_deposit']) ? $data['security_deposit'] : null;
        $model->quantity = isset($data['quantity']) ? $data['quantity'] : null;
        $model->item_condition = 'new';
        $model->item_special_instruction = isset($data['item_special_instruction']) ? $data['item_special_instruction'] : "";
        $model->img_src_payload = (!empty($data['img_src_payload'])) ? json_encode($data['img_src_payload']) : json_encode(array());

        $model->save();
        
        if(!empty($model) && isset($model->id)){
            $ad_detail_mdl = new AdDetail();
            $ad_detail_mdl->ad_type = "article";
            $ad_detail_mdl->article_id = $model->id;
            $ad_detail_mdl->user_id = $data['user_id'];
            $ad_detail_mdl->ad_status = "available";
            $ad_detail_mdl->is_premium_ad = isset($data['is_premium_ad']) ? $data['is_premium_ad'] : 0;
            $ad_detail_mdl->ad_created_on = date('Y-m-d H:i:s');
            $ad_detail_mdl->ad_last_modified = date('Y-m-d H:i:s');
            $ad_detail_mdl->save();

            /*Website users*/
            $is_user_exist = WebsiteUsers::find()
                        ->where(
                            [
                                'id' => $data['user_id']
                            ]
                            )
                        ->one();

            if(!empty($is_user_exist) || ($is_user_exist != null)){
                WebsiteUsers::updateAll(
                                array(
                                  'user_total_ads' => ($is_user_exist->user_total_ads + 1)
                                  ),
                                'id = '.$data['user_id'] 
                            );
            }

            if(!empty($ad_detail_mdl) && isset($ad_detail_mdl->id)){
              $data['ad_detail_id'] = $ad_detail_mdl->id;
            }

        }

        return $data;
    }

    public static function articleDetailUpdate($data){

        $is_article = ArticleDetail::find()
                    ->where(
                        [
                            'id' => $data['id']
                        ]
                        )
                    ->one();

        if(!empty($is_article) || ($is_article != null)){
            ArticleDetail::updateAll(
                            array(
                              'item_name' => isset($data['item_name']) ? $data['item_name'] : $is_article->item_name,
                              'item_category_id' => isset($data['item_category_id']) ? $data['item_category_id'] : $is_article->item_category_id,
                              'item_parent_category_id' => isset($data['item_parent_category_id']) ? $data['item_parent_category_id'] : $is_article->item_parent_category_id,
                              'item_description' => isset($data['item_description']) ? $data['item_description'] : $is_article->item_description,
                              'item_country_id' => isset($data['item_country_id']) ? $data['item_country_id'] : $is_article->item_country_id,
                              'item_city_id' => isset($data['item_city_id']) ? $data['item_city_id'] : $is_article->item_city_id,
                              'item_state_id' => isset($data['item_state_id']) ? $data['item_state_id'] : $is_article->item_state_id,
                              'item_renting_period' => isset($data['item_renting_period']) ? $data['item_renting_period'] : $is_article->item_renting_period,
                              'item_renting_start_date' => isset($data['item_renting_start_date']) ? $data['item_renting_start_date'] : $is_article->item_renting_start_date,
                              'item_renting_end_date' => isset($data['item_renting_end_date']) ? $data['item_renting_end_date'] : $is_article->item_renting_end_date,
                              'item_rent_per_hour' => isset($data['item_rent_per_hour']) ? $data['item_rent_per_hour'] : $is_article->item_rent_per_hour,
                              'item_rent_per_day' => isset($data['item_rent_per_day']) ? $data['item_rent_per_day'] : $is_article->item_rent_per_day,
                              'item_rent_per_week' => isset($data['item_rent_per_week']) ? $data['item_rent_per_week'] : $is_article->item_rent_per_week,
                              'item_rent_per_monthly' => isset($data['item_rent_per_monthly']) ? $data['item_rent_per_monthly'] : $is_article->item_rent_per_monthly,
                              'item_rent_per_year' => isset($data['item_rent_per_year']) ? $data['item_rent_per_year'] : $is_article->item_rent_per_year,
                              'security_deposit' => isset($data['security_deposit']) ? $data['security_deposit'] : $is_article->security_deposit,
                              'quantity' => isset($data['quantity']) ? $data['quantity'] : $is_article->quantity,
                              'item_condition' => isset($data['item_condition']) ? $data['item_condition'] : $is_article->item_condition,
                              'item_special_instruction' => isset($data['item_special_instruction']) ? $data['item_special_instruction'] : $is_article->item_special_instruction,
                              'img_src_payload' => isset($data['img_src_payload']) ? $data['img_src_payload'] : $is_article->img_src_payload,
                              ),
                            'id = '.$data['id'] 
                        );

                /*Update Ad Detail*/
                $is_ad_detail = AdDetail::find()
                            ->where(
                                [
                                    'article_id' => $is_article->id,
                                    'ad_type' => "article"
                                ]
                                )
                            ->one();

                if(!empty($is_ad_detail) || ($is_ad_detail != null)){
                    AdDetail::updateAll(
                                    array(
                                      'user_id' => isset($data['user_id']) ? $data['user_id'] : $is_ad_detail->user_id,
                                      'is_premium_ad' => isset($data['is_premium_ad']) ? $data['is_premium_ad'] : $is_ad_detail->is_premium_ad,
                                      'ad_last_modified' => date('Y-m-d H:i:s')
                                      ),
                                    'id = '.$is_ad_detail->id
                                );
                }
        }

        return $data;
    }
}
