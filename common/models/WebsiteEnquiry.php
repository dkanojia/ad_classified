<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "website_enquiry".
 *
 * @property int $id
 * @property string $enquirer_name
 * @property string $enquirer_email
 * @property int $enquirer_phone
 * @property string $enquiry_desc
 * @property string $enquiry_date
 * @property string $enquiry_status
 * @property int $show_enquiry_to_admin
 * @property string $mark_as_read_on
 * @property string $ip_address
 * @property string $lat_long
 */
class WebsiteEnquiry extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'website_enquiry';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['enquirer_name', 'enquiry_desc', 'enquiry_date', 'enquiry_status', 'show_enquiry_to_admin'], 'required'],
            [[ 'show_enquiry_to_admin'], 'integer'],
            [['enquiry_desc', 'enquiry_status'], 'string'],
            [['enquiry_date', 'mark_as_read_on'], 'safe'],
            [['enquirer_phone','enquirer_name', 'enquirer_email', 'ip_address', 'lat_long'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'enquirer_name' => 'Enquirer Name',
            'enquirer_email' => 'Enquirer Email',
            'enquirer_phone' => 'Enquirer Phone',
            'enquiry_desc' => 'Enquiry Desc',
            'enquiry_date' => 'Enquiry Date',
            'enquiry_status' => 'Enquiry Status',
            'show_enquiry_to_admin' => 'Show Enquiry To Admin',
            'mark_as_read_on' => 'Mark As Read On',
            'ip_address' => 'Ip Address',
            'lat_long' => 'Lat Long',
        ];
    }

    public static function getEnquiry(){
        
        return self::find()->select('id, enquirer_name, enquirer_email, enquirer_phone, enquiry_desc, enquiry_date, mark_as_read_on')
            ->where([
                      'show_enquiry_to_admin' => 1
                    ])
            ->asArray()
            ->all();
        
    }

    public static function updateEnquiry(){
      
      return WebsiteEnquiry::updateAll(
                        array(
                          'mark_as_read_on' => date('Y-m-d H:i:s')
                          ),
                        'mark_as_read_on IS NULL' 
                    );
    
    }

    public static function getEnquirySave($data){
        $model = new WebsiteEnquiry();
        $model->enquirer_name = $data['enquirer_name'];
        $model->enquirer_email = isset($data['enquirer_email']) ? $data['enquirer_email'] : "";
        $model->enquirer_phone = isset($data['enquirer_phone']) ? $data['enquirer_phone'] : "";
        $model->enquiry_desc = $data['enquiry_desc'];
        $model->enquiry_date = date("Y-m-d H:i:s");
        $model->enquiry_status = 'new';
        $model->show_enquiry_to_admin = 1;
        $model->save();

        return $data;
    }

    public static function deleteEnquiry($data){

      if(!isset($data)) return;
      
      $is_enquiry = WebsiteEnquiry::find()
                  ->where(
                      [
                          'id' => $data['id']
                      ]
                      )
                  ->one();

      if(!empty($is_enquiry) || ($is_enquiry != null)){
        WebsiteEnquiry::updateAll(
                        array(
                          'show_enquiry_to_admin' => 0
                          ),
                        'id = '.$data['id'] 
                    );
      }

      return $data;
    }

}
