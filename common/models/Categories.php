<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string $category_name
 * @property string $category_desc
 * @property int $parent_category_id
 * @property string $date_created
 * @property string $date_last_modified
 * @property string $status
 * @property int $total_ads
 * @property int $total_subcategories
 * @property string $category_type
 * @property string $deactivation_date
 * @property int $auto_deactivation
 * @property string $category_icon
 * @property string $category_image
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_name', 'date_created', 'date_last_modified', 'status', 'category_type'], 'required'],
            [['category_desc', 'status', 'category_type'], 'string'],
            [['parent_category_id', 'total_ads', 'total_subcategories', 'auto_deactivation'], 'integer'],
            [['date_created', 'date_last_modified', 'deactivation_date'], 'safe'],
            [['category_name', 'category_icon', 'category_image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_name' => 'Category Name',
            'category_desc' => 'Category Desc',
            'parent_category_id' => 'Parent Category ID',
            'date_created' => 'Date Created',
            'date_last_modified' => 'Date Last Modified',
            'status' => 'Status',
            'total_ads' => 'Total Ads',
            'total_subcategories' => 'Total Subcategories',
            'category_type' => 'Category Type',
            'deactivation_date' => 'Deactivation Date',
            'auto_deactivation' => 'Auto Deactivation',
            'category_icon' => 'Category Icon',
            'category_image' => 'Category Image',
        ];
    }

    public static function getCategories(){

        $query = "select
          categories.id,
          categories.category_name,
          categories.category_desc,
          categories.parent_category_id,
          categories.date_created,
          categories.date_last_modified,
          categories.total_ads,
          categories.total_subcategories,
          categories.category_icon 
        FROM
          categories 
        WHERE parent_category_id = 0 AND status = 'active'";

        $categories  =  Yii::$app->db->createCommand($query)->queryAll();

        $i=0;
        foreach($categories as $p_cat){

            $categories[$i]['child_category'] = self::getSubCategories($p_cat['id']);
            $i++;
        }

        return $categories;
    }

    protected static function getSubCategories($id){
        $query = "select
          categories.id,
          categories.category_name,
          categories.category_desc,
          categories.parent_category_id,
          categories.date_created,
          categories.date_last_modified,
          categories.total_ads,
          categories.total_subcategories,
          categories.category_icon 
        FROM
          categories 
        WHERE status = 'active' AND parent_category_id = ".$id;

        $categories  =  Yii::$app->db->createCommand($query)->queryAll();

        $i=0;
        foreach($categories as $p_cat){

            $categories[$i]['child_category'] = self::getSubCategories($p_cat['id']);
            $i++;
        }

        return $categories;
    }

    public static function addCategory($data){
      $model = new Categories();
      $model->category_name = $data['category_name'];
      $model->category_desc = '';
      $model->parent_category_id = isset($data['parent_category_id']) ? $data['parent_category_id'] : 0;
      $model->date_created = date("Y-m-d H:i:s");
      $model->date_last_modified = date("Y-m-d H:i:s");
      // $model->status = $data['status'];
      $model->status = 'active';
      $model->category_type = 'a';
      $model->category_icon = '';
      $model->category_image = isset($data['category_image']) ? "['".$data['category_image']."']" : "";
      $model->save(); 

      if(!empty($model) && isset($model->id) && isset($data['parent_category_id'])){

        /*Total Sub Categories*/
        $is_exist = Categories::find()
                    ->where(
                        [
                            'id' => $data['parent_category_id']
                        ]
                        )
                    ->one();

        if(!empty($is_exist) || ($is_exist != null)){
            Categories::updateAll(
                            array(
                              'total_subcategories' => ($is_exist->total_subcategories + 1 )
                              ),
                            'id = '.$data['parent_category_id'] 
                        );
        }
      
      }

      return $data;
    }

    public static function categoryUpdate($data){
      
      $is_category = Categories::find()
                  ->where(
                      [
                          'id' => $data['id']
                      ]
                      )
                  ->one();

      if(!empty($is_category) || ($is_category != null)){
          Categories::updateAll(
                          array(
                            'category_name' => isset($data['category_name']) ? $data['category_name'] : $is_category->category_name,
                            // 'category_desc' => isset($data['category_desc']) ? $data['category_desc'] : $is_category->category_desc,
                            'parent_category_id' => isset($data['parent_category_id']) ? $data['parent_category_id'] : $is_category->parent_category_id,
                            'date_last_modified' => date("Y-m-d H:i:s"),
                            // 'status' => isset($data['status']) ? $data['status'] : $is_category->status,
                            // 'category_type' => isset($data['category_type']) ? $data['category_type'] : $is_category->category_type,
                            // 'category_icon' => isset($data['category_icon']) ? $data['category_icon'] : $is_category->category_icon,
                            // 'category_image' => isset($data['category_image']) ? $data['category_image'] : $is_category->category_image
                            ),
                          'id = '.$data['id'] 
                      );
        }

        return $data;
    }

    public static function categoryDelete($data){

      if(!isset($data)) return;
      
      $is_category = Categories::find()
                  ->where(
                      [
                          'id' => $data['id']
                      ]
                      )
                  ->one();

      if(!empty($is_category) || ($is_category != null)){
        Categories::updateAll(
                        array(
                          'status' => 'inactive',
                          'deactivation_date' => date("Y-m-d H:i:s")
                          ),
                        'id = '.$data['id'] 
                    );
      }

      return $data;
    }
}
