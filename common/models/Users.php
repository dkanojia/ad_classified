<?php

namespace common\models;

use Yii;
use common\helpers\Constants;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $mobile_no
 * @property string $otp
 * @property string $created
 */
use app\models\States;
use app\models\Corps;
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name'], 'required'],
            [['created'], 'safe'],
            [['otp'], 'string'],
            [['mobile_no'], 'unique'],
            [['first_name', 'last_name'], 'string', 'max' => 50],
            [['mobile_no'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'mobile_no' => 'Mobile No',
            'created' => 'Created',
        ];
    }

    public function generateOTP() {
        $string = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string_shuffled = str_shuffle($string);
        return substr($string_shuffled, 1, 4);
    }

    public static function generateRandomKey() {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $token = '';
        for ($i = 0; $i < 20; ++$i) {
            $token .= $characters[rand(0, $charactersLength - 1)];
        }
        return $token;
    }

    public static function verifyHeaders($headers, $required = true){
        if($headers->has('secret-key') && $headers->has('access-token')){
            $secret_key = $headers->get('secret-key');
            $access_token = $headers->get('access-token');
            
            if(empty($access_token) && $required)
                return ["Ack" => "Error", 'code' => 401, 'Message' =>  'Invalid request due to headers'];
            $record_id = self::find()->select('id')->where(['access_token' => $access_token])->one();
            if(!empty(trim($secret_key))){
                if($secret_key != Constants::SECRET_KEY){                
                    return ["Ack" => "Error", 'code' => 401, 'Message' =>  'Invalid secret key'];
                } elseif(empty($record_id) && $required) {
                    return ["Ack" => "Error", 'code' => 419, 'Message' => 'Access token invalid'];
                }
            }else{
                return ["Ack" => "Error", 'code' => 401, 'Message' =>  'Invalid request due to headers'];
            }
        } else{            
            return ["Ack" => "Error", 'code' => 401, 'Message' =>  'Invalid request due to headers, access token or secret key is not available'];
        }        
        
        return ["Ack" => "Error", 'code' => 200, 'Message' => 'Authorization accepted'];
    }

    /**
     * Sends an email with a link, for login.
     *
     * @return otp
     */
    public function sendOTP($mobile_no = false, $type = false)
    {        
        try{

            $query = self::find();

            $query->where(['mobile_no' => $mobile_no]);
            $user = $query->one();
            if ($user) {
                $user->otp = $this->generateOTP();
                $user->save();
            }
            else{
                throw new \ErrorException("Mobile number is not found.");
            }
            
            //SMS the OTP
            $message = "Your Pioneer OTP is: ".$user->otp;
            // SMS::sendMessage($mobile, $message);
            
            return $user->otp;
        }
        catch(\Exception $e){
            $response = ["Ack" => "Error", "Message" => $e->getMessage(), "Code" => $e->getFile(), "Line" => $e->getLine()];
        }
    }

    /**
     * Creates a form model given a otp.
     *
     * @param  string $otp
     * @param  array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if otp is empty or not valid
     */
    public function validateOtp($otp, $mobile)
    {
        try{
            if (empty($otp) || !is_string($otp)) {
                throw new InvalidParamException('Password reset OTP cannot be blank.');
            }

            
            $query = self::find();

            $query->where(['mobile_no' => $mobile, 'otp' => $otp]);
            $user = $query->one();
            
            if($user){

                $corps = Corps::find()->select('id, name')->asArray()->all();
                $access_token = $this->generateRandomKey();

                // save access_token to users db
                $user->access_token = $access_token;
                $user->save();

                return ['crops' => $corps, 'access-token' => $access_token];
            }
            else{
                throw new \ErrorException("Mobile number is not found.");
            }
        }
        catch(\Exception $e){
            $response = ["Ack" => "Error", "Message" => $e->getMessage(), "Code" => $e->getFile(), "Line" => $e->getLine()];
        }        
    }
}
