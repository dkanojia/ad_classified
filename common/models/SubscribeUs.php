<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "subscribe_us".
 *
 * @property int $id
 * @property string $email
 * @property string $date
 */
class SubscribeUs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subscribe_us';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['date'], 'safe'],
            [['email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'date' => 'Date',
        ];
    }

    /*
    Saving Enquiry
    */
    public static function putSubscribeUsSave($data){

        $is_exist = SubscribeUs::find()
                    ->where(
                        [
                            'email' => $data['email']
                        ]
                        )
                    ->one();

        if(!empty($is_exist) || ($is_exist != null)){
            SubscribeUs::updateAll(
                            array('email' => $data['email']),  'email = '."'".$data['email']."'" 
                        ); 
        }else{
            $model = new SubscribeUs();
            $model->email = $data['email'];
            $model->date = date("Y-m-d H:i:s");
            $model->save();
        }

        return $data;
    }
}
