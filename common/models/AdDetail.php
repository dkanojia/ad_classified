<?php

namespace common\models;

use Yii;
use common\models\AdManagment;

/**
 * This is the model class for table "ad_detail".
 *
 * @property int $id
 * @property string $ad_type
 * @property int $article_id
 * @property int $user_id
 * @property int $total_views
 * @property int $total_requests
 * @property string $ad_status
 * @property int $ad_blocked
 * @property int $ad_deactivated
 * @property int $is_premium_ad
 * @property string $ad_created_on
 * @property string $ad_last_modified
 * @property int $ad_repeated
 * @property int $ad_count_offensive
 * @property int $ad_count_bad_classified
 * @property int $ad_count_reported
 * @property int $is_ad_deleted
 */
class AdDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ad_type', 'article_id', 'user_id', 'ad_status', 'ad_created_on', 'ad_last_modified'], 'required'],
            [['ad_type', 'ad_status'], 'string'],
            [['article_id', 'user_id', 'total_views', 'total_requests', 'ad_blocked', 'ad_deactivated', 'is_premium_ad', 'ad_repeated', 'ad_count_offensive', 'ad_count_bad_classified', 'ad_count_reported', 'is_ad_deleted'], 'integer'],
            [['ad_created_on', 'ad_last_modified'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ad_type' => 'Ad Type',
            'article_id' => 'Article ID',
            'user_id' => 'User ID',
            'total_views' => 'Total Views',
            'total_requests' => 'Total Requests',
            'ad_status' => 'Ad Status',
            'ad_blocked' => 'Ad Blocked',
            'ad_deactivated' => 'Ad Deactivated',
            'is_premium_ad' => 'Is Premium Ad',
            'ad_created_on' => 'Ad Created On',
            'ad_last_modified' => 'Ad Last Modified',
            'ad_repeated' => 'Ad Repeated',
            'ad_count_offensive' => 'Ad Count Offensive',
            'ad_count_bad_classified' => 'Ad Count Bad Classified',
            'ad_count_reported' => 'Ad Count Reported',
            'is_ad_deleted' => 'Is Ad Deleted',
        ];
    }

    public static function getList()
    {
        $query = "select ad_detail.id, ad_detail.ad_type, article_detail.item_name, article_detail.item_category_id, article_detail.item_parent_category_id, article_detail.item_description, cities.name as city, states.name as state, countries.name as country, article_detail.item_special_instruction, article_detail.item_rent_per_hour, article_detail.item_rent_per_day, article_detail.item_rent_per_week, article_detail.item_rent_per_monthly, article_detail.item_rent_per_year, article_detail.security_deposit, article_detail.quantity, website_users.user_name, website_users.user_phone, website_users.user_email, article_detail.img_src_payload, ad_detail.ad_created_on, ad_detail.ad_last_modified, ad_detail.ad_status, ad_detail.ad_blocked, ad_detail.ad_deactivated, ad_detail.is_ad_deleted, ad_detail.total_views, ad_detail.total_requests,
          parentCategories.category_name as parent_category_name,
          categories.category_name
          from ad_detail
          
          LEFT JOIN article_detail ON ad_detail.article_id = article_detail.id
          LEFT JOIN cities ON article_detail.item_city_id = cities.id
          LEFT JOIN states ON article_detail.item_state_id = states.id
          LEFT JOIN countries ON article_detail.item_country_id = countries.id
          
          
          LEFT JOIN categories ON categories.id = article_detail.item_category_id
          LEFT JOIN categories parentCategories ON parentCategories.id = article_detail.item_parent_category_id
          
          LEFT JOIN website_users ON ad_detail.user_id = website_users.id
          WHERE ad_detail.is_ad_deleted = 0
          ORDER BY ad_detail.ad_created_on DESC";
        // $query->andWhere(['IS NOT', 'id', NULL]);
        // $query->orderBy('id Desc');
        $users  =  Yii::$app->db->createCommand($query)->queryAll();
        return $users;
    }

    public static function getListForCustomer($data)
    {
        $where_string = "";
        $key_name = "";

        if(isset($data['searchTerm'])) {
            $where_string .=  "WHERE article_detail.item_name LIKE '%".$data['searchTerm']."%'";
            $key_name .= "A";
        }

        if(isset($data['cityID'])) {
            $key_name .= "B";
            
            if($key_name == 'AB'){
                $where_string .=  " AND cities.id = ".$data['cityID']." ";
            }else{
                $where_string .=  " WHERE cities.id = ".$data['cityID']." ";
            }

        }

        if(isset($data['categoryID'])) {
            $key_name .= "C";
            
            if(($key_name == 'ABC') ||  ($key_name == 'AC') ||  ($key_name == 'BC')){
                $where_string .=  " AND categories.id = ".$data['categoryID']." ";
            }else{
                $where_string .=  " WHERE categories.id = ".$data['categoryID']." ";
            }

        }

        // Pagination
        $pageSize = isset($data['pageSize']) ? $data['pageSize'] : 20;
        $currentPage = isset($data['currentPage']) ? $data['currentPage'] : 1;
        $offset_value = ( ($pageSize * $currentPage) - $pageSize);

        $query = "select
          tbl_ad_detail.id AS ad_id,
          
          article_detail.id AS article_id,
          article_detail.item_name,

          article_detail.item_category_id,
          categories.category_name,

          article_detail.item_parent_category_id,
          parentCategories.category_name as parent_category_name,

          article_detail.item_description,

          article_detail.item_city_id,
          cities.name as city,

          article_detail.item_state_id,
          states.name as state,

          countries.name as country,

          article_detail.item_rent_per_hour,
          article_detail.item_rent_per_day,
          article_detail.item_rent_per_week,
          article_detail.item_rent_per_monthly,
          article_detail.item_rent_per_year,

          article_detail.security_deposit,
          article_detail.quantity,

          article_detail.item_special_instruction,

          article_detail.img_src_payload,

          tbl_website_user.user_name,
          tbl_website_user.user_email,
          tbl_website_user.user_phone,
          tbl_website_user.user_profile_img,

          tbl_ad_detail.article_id,
          tbl_ad_detail.user_id,
          tbl_ad_detail.ad_status,
          tbl_ad_detail.ad_created_on,
          tbl_ad_detail.total_views
        FROM
          (
          SELECT
            *
          FROM
            ad_detail
          WHERE
            ad_deactivated = 0 AND ad_blocked = 0 AND is_ad_deleted = 0
        ) tbl_ad_detail
        JOIN
          article_detail ON tbl_ad_detail.article_id = article_detail.id
        JOIN
          (
          SELECT
            *
          FROM
            website_users
          WHERE
            user_blocked = 0 AND user_deactivated = 0
        ) tbl_website_user ON tbl_ad_detail.user_id = tbl_website_user.id
        JOIN
          cities ON cities.id = article_detail.item_city_id
        JOIN
          states ON states.id = article_detail.item_state_id
        JOIN
          countries ON countries.id = article_detail.item_country_id
        JOIN
          categories ON categories.id = article_detail.item_category_id
        JOIN
          categories parentCategories ON parentCategories.id = article_detail.item_parent_category_id 
          ".$where_string."
          ORDER BY tbl_ad_detail.ad_created_on DESC
          LIMIT ".$offset_value.",".$pageSize."
          ";

        $ads_list  =  Yii::$app->db->createCommand($query)->queryAll();
        return $ads_list;
    }

    public static function getAdListForParentCategory($data)
    {
        // Pagination
        $pageSize = isset($data['pageSize']) ? $data['pageSize'] : 20;
        $currentPage = isset($data['currentPage']) ? $data['currentPage'] : 1;
        $offset_value = ( ($pageSize * $currentPage) - $pageSize);

        $query = "select
          cities.name,
          states.name,
          countries.name,

          categories.category_name,
          parentCategories.category_name,

          tbl_ad_detail.id AS ad_id,
          tbl_ad_detail.ad_created_on,
          
          article_detail.id AS pk_article_id,
          article_detail.item_name,
          article_detail.item_category_id,
          article_detail.item_parent_category_id,
          article_detail.item_description,

          article_detail.item_city_id,
          cities.name as city,

          article_detail.item_state_id,
          states.name as state,

          countries.name as country,

          article_detail.item_rent_per_day,
          article_detail.item_rent_per_week,
          article_detail.item_rent_per_hour,
          article_detail.item_rent_per_monthly,
          article_detail.item_rent_per_year,
          article_detail.security_deposit,
          article_detail.quantity,
          article_detail.item_special_instruction,
          article_detail.img_src_payload,
          tbl_website_user.user_name,
          tbl_website_user.user_email,
          tbl_website_user.user_phone,
          tbl_website_user.user_profile_img

        FROM
          (
          SELECT
            *
          FROM
            ad_detail
          WHERE
            ad_deactivated = 0 AND ad_blocked = 0 AND is_ad_deleted = 0
        ) tbl_ad_detail
        JOIN
          article_detail ON tbl_ad_detail.article_id = article_detail.id
        JOIN
          (
          SELECT
            *
          FROM
            website_users
          WHERE
            user_blocked = 0 AND user_deactivated = 0
        ) tbl_website_user ON tbl_ad_detail.user_id = tbl_website_user.id
        JOIN
          cities ON cities.id = article_detail.item_city_id
        JOIN
          states ON states.id = article_detail.item_state_id
        JOIN
          countries ON countries.id = article_detail.item_country_id
        JOIN
          categories ON categories.id = article_detail.item_parent_category_id
        JOIN
          categories parentCategories ON parentCategories.id = article_detail.item_parent_category_id 
        WHERE 
            categories.id = ".$data['parentCategoryID']."
        ORDER BY tbl_ad_detail.ad_created_on DESC
        LIMIT ".$offset_value.",".$pageSize."
        ";
        $ads_list  =  Yii::$app->db->createCommand($query)->queryAll();
        return $ads_list;
    }

    public static function getSuggestionListForCustomer($data)
    {
        // Pagination
        $pageSize = isset($data['pageSize']) ? $data['pageSize'] : 20;
        $currentPage = isset($data['currentPage']) ? $data['currentPage'] : 1;
        $offset_value = ( ($pageSize * $currentPage) - $pageSize);

        $query = "select
          cities.name,
          states.name,
          countries.name,

          categories.category_name,
          parentCategories.category_name,

          tbl_ad_detail.id AS pk_ad_id,
          
          article_detail.id AS pk_article_id,
          article_detail.item_name,
          article_detail.item_category_id,
          article_detail.item_parent_category_id,
          article_detail.item_description,
          article_detail.item_city_id,
          article_detail.item_state_id,
          article_detail.item_rent_per_day,
          article_detail.item_rent_per_week,
          article_detail.item_rent_per_hour,
          article_detail.item_rent_per_monthly,
          article_detail.item_rent_per_year,
          article_detail.security_deposit,
          article_detail.quantity,
          article_detail.item_special_instruction,
          article_detail.img_src_payload,
          tbl_website_user.user_name,
          tbl_website_user.user_email,
          tbl_website_user.user_phone,
          tbl_website_user.user_profile_img

        FROM
          (
          SELECT
            *
          FROM
            ad_detail
          WHERE
            ad_deactivated = 0 AND ad_blocked = 0 AND is_ad_deleted = 0
        ) tbl_ad_detail
        JOIN
          article_detail ON tbl_ad_detail.article_id = article_detail.id
        JOIN
          (
          SELECT
            *
          FROM
            website_users
          WHERE
            user_blocked = 0 AND user_deactivated = 0
        ) tbl_website_user ON tbl_ad_detail.user_id = tbl_website_user.id
        JOIN
          cities ON cities.id = article_detail.item_city_id
        JOIN
          states ON states.id = article_detail.item_state_id
        JOIN
          countries ON countries.id = article_detail.item_country_id
        JOIN
          categories ON categories.id = article_detail.item_parent_category_id
        JOIN
          categories parentCategories ON parentCategories.id = article_detail.item_parent_category_id 
        WHERE 
            categories.id = ".$data['parentCategoryID']."
        ORDER BY tbl_ad_detail.ad_created_on DESC
        LIMIT ".$offset_value.",".$pageSize."
        ";
        $ads_list  =  Yii::$app->db->createCommand($query)->queryAll();
        return $ads_list;
    }

    public static function getUserAdsList($data)
    {
        // Pagination
        $pageSize = isset($data['pageSize']) ? $data['pageSize'] : 20;
        $currentPage = isset($data['currentPage']) ? $data['currentPage'] : 1;
        $offset_value = ( ($pageSize * $currentPage) - $pageSize);

        $query = "select
            tbl_ad_detail.article_id,
            tbl_ad_detail.ad_status,
            tbl_ad_detail.ad_deactivated,
            tbl_ad_detail.ad_created_on,
            tbl_ad_detail.total_views,

            tbl_ad_detail.user_id,
            tbl_website_user.user_name,
            
            article_detail.item_name,
            
            article_detail.id AS pk_article_id,
            article_detail.item_category_id,
            categories.category_name as child_category,

            article_detail.item_parent_category_id,
            parentCategories.category_name as parent_category,

            article_detail.item_description,
            article_detail.item_city_id,
            cities.name as city,

            article_detail.item_state_id,
            states.name as state,

            article_detail.item_country_id,
            countries.name as country,

            article_detail.item_rent_per_hour,
            article_detail.item_rent_per_day,
            article_detail.item_rent_per_week,
            article_detail.item_rent_per_monthly,
            article_detail.item_rent_per_year,
            article_detail.security_deposit,
            article_detail.quantity,

            article_detail.item_special_instruction,
            article_detail.img_src_payload,

            tbl_ad_detail.id AS pk_ad_id
          FROM
            (
            SELECT
              *
            FROM
              ad_detail
            WHERE
              is_ad_deleted = 0 AND ad_blocked = 0
          ) tbl_ad_detail
          JOIN
            article_detail ON tbl_ad_detail.article_id = article_detail.id
          JOIN
            (
            SELECT
              *
            FROM
              website_users
            WHERE
              user_blocked = 0 AND user_deactivated = 0
          ) tbl_website_user ON tbl_ad_detail.user_id = tbl_website_user.id AND tbl_ad_detail.user_id = ".$data['user_id']."
          JOIN
            cities ON cities.id = article_detail.item_city_id
          JOIN
            states ON states.id = article_detail.item_state_id
          JOIN
            countries ON countries.id = article_detail.item_country_id
          JOIN
            categories ON categories.id = article_detail.item_category_id
          JOIN
            categories parentCategories ON parentCategories.id = article_detail.item_parent_category_id
          ORDER BY tbl_ad_detail.ad_created_on DESC
          LIMIT ".$offset_value.",".$pageSize."
          ";

        $ads_list  =  Yii::$app->db->createCommand($query)->queryAll();
        return $ads_list;
    }

    public static function getDetails($data)
    {
        $id = $data['id'];

        $query = "select
              tbl_ad_detail.id as ad_id,
              tbl_ad_detail.user_id,
              tbl_ad_detail.ad_status,
              tbl_ad_detail.ad_created_on,
              
              tbl_article_detail.item_name,
              tbl_article_detail.item_category_id,
              categories.category_name as child_category,
              tbl_article_detail.item_parent_category_id,
              parentCategories.category_name as parent_category,
              tbl_article_detail.item_description,
              tbl_ad_detail.total_views,

              tbl_article_detail.item_city_id,
              tbl_cities.name as city,
              
              tbl_article_detail.item_state_id,
              states.name as state,

              tbl_article_detail.item_country_id,
              countries.name as country,

              tbl_article_detail.item_rent_per_hour,
              tbl_article_detail.item_rent_per_day,
              tbl_article_detail.item_rent_per_week,
              tbl_article_detail.item_rent_per_monthly,
              tbl_article_detail.item_rent_per_year,
              
              tbl_article_detail.security_deposit,
              tbl_article_detail.quantity,
              tbl_article_detail.item_special_instruction,
              
              tbl_article_detail.img_src_payload,
              tbl_website_user.user_name

            FROM
              (
              SELECT
                *
              FROM
                ad_detail
              WHERE
                id = '".$id."' AND ad_deactivated = 0 AND ad_blocked = 0
            ) tbl_ad_detail
            JOIN
              article_detail AS tbl_article_detail ON tbl_ad_detail.article_id = tbl_article_detail.id
            JOIN
              (
              SELECT
                *
              FROM
                website_users
              WHERE
                user_blocked = 0 AND user_deactivated = 0
            ) tbl_website_user ON tbl_ad_detail.user_id = tbl_website_user.id
            JOIN
              cities AS tbl_cities ON tbl_cities.id = tbl_article_detail.item_city_id
            JOIN
              states ON states.id = tbl_article_detail.item_state_id
            JOIN
              countries ON countries.id = tbl_article_detail.item_country_id
            JOIN
              categories ON categories.id = tbl_article_detail.item_category_id
            JOIN
              categories parentCategories ON parentCategories.id = tbl_article_detail.item_parent_category_id";
        // $query->andWhere(['IS NOT', 'id', NULL]);
        // $query->orderBy('id Desc');
        $ads_detail  =  Yii::$app->db->createCommand($query)->queryAll();

        $is_exist = AdDetail::find()
                    ->where(
                        [
                            'id' => $data['id']
                        ]
                        )
                    ->one();

        if(!empty($is_exist) || ($is_exist != null)){
            AdDetail::updateAll(
                            array(
                              'total_views' => ($is_exist->total_views + 1 )
                              ),
                            'id = '.$data['id'] 
                        );
        }

        return $ads_detail;
    }


    public static function getDetailsForEdit($data)
    {
        $id = $data['id'];

        $query = "select
              tbl_ad_detail.id as ad_id,
              tbl_ad_detail.user_id,
              tbl_ad_detail.ad_status,
              tbl_ad_detail.ad_created_on,
              
              tbl_article_detail.item_name,
              tbl_article_detail.item_category_id,
              categories.category_name as child_category,
              tbl_article_detail.item_parent_category_id,
              parentCategories.category_name as parent_category,
              tbl_article_detail.item_description,
              tbl_ad_detail.total_views,

              tbl_article_detail.item_city_id,
              tbl_cities.name as city,
              
              tbl_article_detail.item_state_id,
              states.name as state,

              tbl_article_detail.item_country_id,
              countries.name as country,

              tbl_article_detail.item_rent_per_hour,
              tbl_article_detail.item_rent_per_day,
              tbl_article_detail.item_rent_per_week,
              tbl_article_detail.item_rent_per_monthly,
              tbl_article_detail.item_rent_per_year,
              
              tbl_article_detail.security_deposit,
              tbl_article_detail.quantity,
              tbl_article_detail.item_special_instruction,
              
              tbl_article_detail.img_src_payload,
              tbl_website_user.user_name

            FROM
              (
              SELECT
                *
              FROM
                ad_detail
              WHERE
                id = '".$id."' AND ad_deactivated = 0 AND ad_blocked = 0
            ) tbl_ad_detail
            JOIN
              article_detail AS tbl_article_detail ON tbl_ad_detail.article_id = tbl_article_detail.id
            JOIN
              (
              SELECT
                *
              FROM
                website_users
              WHERE
                user_blocked = 0 AND user_deactivated = 0
            ) tbl_website_user ON tbl_ad_detail.user_id = tbl_website_user.id
            JOIN
              cities AS tbl_cities ON tbl_cities.id = tbl_article_detail.item_city_id
            JOIN
              states ON states.id = tbl_article_detail.item_state_id
            JOIN
              countries ON countries.id = tbl_article_detail.item_country_id
            JOIN
              categories ON categories.id = tbl_article_detail.item_category_id
            JOIN
              categories parentCategories ON parentCategories.id = tbl_article_detail.item_parent_category_id";
        // $query->andWhere(['IS NOT', 'id', NULL]);
        // $query->orderBy('id Desc');
        $ads_detail  =  Yii::$app->db->createCommand($query)->queryAll();

        return $ads_detail;
    }

    public static function getUserRequestedAdsList($data){
      
      // Pagination
      $pageSize = isset($data['pageSize']) ? $data['pageSize'] : 20;
      $currentPage = isset($data['currentPage']) ? $data['currentPage'] : 1;
      $offset_value = ( ($pageSize * $currentPage) - $pageSize);

      $query = "select
      tbl_ad_managment.id AS request_id,
      tbl_ad_managment.rentor_id,
      tbl_ad_managment.rentee_id,
      tbl_ad_managment.ad_id,
      tbl_ad_managment.request_status,
      tbl_ad_managment.date_requested,
      tbl_ad_managment.date_resolved,

      tbl_ad_managment.is_ad_deleted,

      tbl_ad_detail.id AS pk_ad_id,
      tbl_ad_detail.user_id,
      tbl_ad_detail.ad_status,

      tbl_ad_detail.ad_created_on,

      tbl_website_user.user_name,
      tbl_website_user.user_email,
      tbl_website_user.user_phone,

      article_detail.id AS pk_article_id,
      article_detail.item_name,
      article_detail.item_category_id,
      categories.category_name as child_category,

      article_detail.item_parent_category_id,
      parentCategories.category_name as parent_category,

      article_detail.item_description,
      article_detail.item_city_id,
      cities.name as city,

      article_detail.item_state_id,
      states.name as state,

      article_detail.item_country_id,
      countries.name as country,

      article_detail.security_deposit,
      article_detail.quantity,
      article_detail.item_special_instruction,
      article_detail.img_src_payload,

      article_detail.item_rent_per_day,
      article_detail.item_rent_per_week,
      article_detail.item_rent_per_monthly,
      article_detail.item_rent_per_year,

      tbl_website_user.user_name
    FROM
      (
      SELECT
        *
      FROM
        ad_managment
      WHERE
        rentee_id =  ".$data['user_id']." AND is_ad_blocked = 0
    ) AS tbl_ad_managment
    JOIN
      (
    SELECT
      *
    FROM
      ad_detail
    ) tbl_ad_detail ON tbl_ad_detail.id = tbl_ad_managment.ad_id
    JOIN
      article_detail ON tbl_ad_detail.article_id = article_detail.id
    JOIN
      (
      SELECT
        *
      FROM
        website_users
      WHERE
        user_blocked = 0
    ) tbl_website_user ON tbl_ad_detail.user_id = tbl_website_user.id
    JOIN
      cities ON cities.id = article_detail.item_city_id
    JOIN
      states ON states.id = article_detail.item_state_id
    JOIN
      countries ON countries.id = article_detail.item_country_id
    JOIN
      categories ON categories.id = article_detail.item_category_id
    JOIN
      categories parentCategories ON parentCategories.id = article_detail.item_parent_category_id
        ORDER BY tbl_ad_managment.date_requested DESC
        LIMIT ".$offset_value.",".$pageSize."
        ";

      $ads_list  =  Yii::$app->db->createCommand($query)->queryAll();
      return $ads_list;


    }

    public static function getRequestListForSpecificAd($data){
      // Pagination
      $pageSize = isset($data['pageSize']) ? $data['pageSize'] : 100;
      $currentPage = isset($data['currentPage']) ? $data['currentPage'] : 1;
      $offset_value = ( ($pageSize * $currentPage) - $pageSize);

      $query = "select
        tbl_ad_managment.id AS ad_managment_id,
        tbl_ad_managment.rentor_id,
        tbl_ad_managment.rentee_id,
        tbl_ad_managment.ad_id,
        tbl_ad_managment.rentee_message,
        tbl_ad_managment.request_status,
        tbl_ad_managment.date_requested,
        tbl_ad_detail.id AS pk_ad_id,
        tbl_ad_detail.user_id,
        tbl_website_user.user_name,
        tbl_website_user.user_email

      FROM
        (
        SELECT
          *
        FROM
          ad_managment
        WHERE
          rentor_id = ".$data['rentor_id']." AND ad_id = ".$data['ad_id']." AND request_status = 'in_queue' AND is_ad_blocked = 0 AND is_ad_deleted = 0
      ) AS tbl_ad_managment
      JOIN
        (
        SELECT
          *
        FROM
          ad_detail
        WHERE
          is_ad_deleted = 0 AND ad_deactivated = 0 AND ad_blocked = 0
      ) tbl_ad_detail ON tbl_ad_detail.id = tbl_ad_managment.ad_id
      JOIN
        (
        SELECT
          *
        FROM
          website_users
        WHERE
          user_blocked = 0 AND user_deactivated = 0
      ) tbl_website_user ON tbl_ad_managment.rentee_id = tbl_website_user.id

        ORDER BY tbl_ad_managment.date_requested DESC
        LIMIT ".$offset_value.",".$pageSize."
        ";

      $ads_list  =  Yii::$app->db->createCommand($query)->queryAll();
      return $ads_list;

    }

    public static function putAdDeactive($data){
      
      if(!isset($data)) return;
      
      AdDetail::updateAll(
                        array('ad_deactivated' => $data['active']),  'id = '.$data['ad_id'] 
                        );

      return $data;

    }

    public static function putAdDelete($data){

      if(!isset($data)) return;

      $is_ad_detail_updated = AdDetail::updateAll(
                        array('is_ad_deleted' => 1),  'id = '.$data['ad_id'] 
                        );

      if($is_ad_detail_updated == 1){
        $is_exist = AdDetail::find()
                    ->where(
                        [
                            'id' => $data['ad_id']
                        ]
                        )
                    ->one();

        if(!empty($is_exist) || ($is_exist != null)){
          $ad_managment = AdManagment::find()
              ->where(
                  [
                      'ad_id' => $data['ad_id']
                  ]
                  )
              ->one();

          if(!empty($ad_managment) || ($ad_managment != null)){
              AdManagment::updateAll(
                          array('is_ad_deleted' => 1),  ' ad_id = '.$data['ad_id'] 
                          );   

          }
        }
      }

      return $data;
    }

    public static function getRequestsResolvedByRentor($data){
      
      // Pagination
      $pageSize = isset($data['pageSize']) ? $data['pageSize'] : 20;
      $currentPage = isset($data['currentPage']) ? $data['currentPage'] : 1;
      $offset_value = ( ($pageSize * $currentPage) - $pageSize);

      $query = "select
      tbl_ad_managment.id AS request_id,
      tbl_ad_managment.rentor_id,
      tbl_ad_managment.rentee_id,
      tbl_ad_managment.ad_id,
      tbl_ad_managment.request_status,
      tbl_ad_managment.date_requested,
      tbl_ad_managment.date_resolved,

      tbl_ad_managment.is_ad_deleted,

      tbl_ad_detail.id AS pk_ad_id,
      tbl_ad_detail.user_id,
      tbl_ad_detail.ad_status,

      tbl_ad_detail.ad_created_on,

      tbl_website_user.user_name,
      tbl_website_user.user_email,
      tbl_website_user.user_phone,

      article_detail.id AS pk_article_id,
      article_detail.item_name,
      article_detail.item_category_id,
      categories.category_name as child_category,

      article_detail.item_parent_category_id,
      parentCategories.category_name  as parent_category,

      article_detail.item_description,
      article_detail.item_city_id,
      cities.name as city,

      article_detail.item_state_id,
      states.name as state,

      article_detail.item_country_id,
      countries.name as country,

      article_detail.security_deposit,
      article_detail.quantity,
      article_detail.item_special_instruction,
      article_detail.img_src_payload
    FROM
      (
      SELECT
        *
      FROM
        ad_managment
      WHERE
        rentor_id =  ".$data['user_id']." AND request_status != 'in_queue' AND is_ad_blocked = 0
    ) AS tbl_ad_managment
    JOIN
      (
    SELECT
      *
    FROM
      ad_detail
    ) tbl_ad_detail ON tbl_ad_detail.id = tbl_ad_managment.ad_id
    JOIN
      article_detail ON tbl_ad_detail.article_id = article_detail.id
    JOIN
      (
      SELECT
        *
      FROM
        website_users
      WHERE
        user_blocked = 0 AND user_deactivated = 0
    ) tbl_website_user ON tbl_ad_managment.rentee_id = tbl_website_user.id
    JOIN
      cities ON cities.id = article_detail.item_city_id
    JOIN
      states ON states.id = article_detail.item_state_id
    JOIN
      countries ON countries.id = article_detail.item_country_id
    JOIN
      categories ON categories.id = article_detail.item_category_id
    JOIN
      categories parentCategories ON parentCategories.id = article_detail.item_parent_category_id
        ORDER BY tbl_ad_managment.date_requested DESC
        LIMIT ".$offset_value.",".$pageSize."
        ";

      $ads_list  =  Yii::$app->db->createCommand($query)->queryAll();
      return $ads_list;
    }


    public static function websiteAdBlockUnBlock($data){
        $web_ads = AdDetail::find()
            ->where(
                    [
                        'id' => $data['website_ad_id']
                    ]
                )
            ->one();

        if(!empty($web_ads) || ($web_ads != null)){
            $website_ad_id = AdDetail::updateAll(
                            array('ad_blocked' => $data['new_status']),  'id = '.$data['website_ad_id'] 
                        );
        }
        return $data;
    }

    public static function getDeactivatedAdList($data){
      return self::find()
              ->where(['ad_deactivated' => 1, 'user_id' => $data['user_id']])
              ->asArray()
              ->all();
    }

}
