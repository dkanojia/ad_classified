<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "admin_user".
 *
 * @property int $userid
 * @property string $firstName
 * @property string $lastName
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property int $roleId
 * @property string $status
 * @property string $lastloggedin
 * @property string $createdDate
 * @property string $modifiedDate
 * @property string $access_token
 * @property string $deletedAccountDate
 */
class AdminUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstName', 'lastName', 'email', 'password', 'phone', 'roleId', 'status', 'lastloggedin', 'createdDate', 'modifiedDate', 'access_token', 'deletedAccountDate'], 'required'],
            [['roleId'], 'integer'],
            [['status'], 'string'],
            [['lastloggedin', 'createdDate', 'modifiedDate', 'deletedAccountDate'], 'safe'],
            [['firstName', 'lastName', 'phone'], 'string', 'max' => 20],
            [['email'], 'string', 'max' => 100],
            [['password'], 'string', 'max' => 200],
            [['access_token'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'userid' => 'Userid',
            'firstName' => 'First Name',
            'lastName' => 'Last Name',
            'email' => 'Email',
            'password' => 'Password',
            'phone' => 'Phone',
            'roleId' => 'Role ID',
            'status' => 'Status',
            'lastloggedin' => 'Lastloggedin',
            'createdDate' => 'Created Date',
            'modifiedDate' => 'Modified Date',
            'access_token' => 'Access Token',
            'deletedAccountDate' => 'Deleted Account Date',
        ];
    }

    public static function checkAuth($email,$password)
    {
        $password = hash('gost', $password);
        if(self::findOne(['email' => $email,'password' => $password])){          
            $access_token = bin2hex(random_bytes(10));  
            AdminUser::updateAll(array('access_token' => $access_token,'status' => 'Y','lastloggedin' => date('Y-m-d H:i:s')),'password = "'.$password.'"');
            return $access_token;
        }
        return false;
    }

    public static function logout()
    {
        AdminUser::updateAll(array('status' => 'N', 'access_token' => NULL));
        return true;
    }

    public static function verifyHeaders($headers, $required = true){
        if($headers->has('access-token')){
            $access_token = $headers->get('access-token');
            if(empty($access_token) && $required)
                return ["Ack" => "Error", 'code' => 401, 'Message' =>  'Invalid request due to headers'];
            $record_id = self::find()->select('userid')->where(['access_token' => $access_token,'status' => 'Y'])->one();
            if($record_id){
                return ["Ack" => "Error", 'code' => 200, 'Message' => 'Authorization accepted'];
            }else{
                return ["Ack" => "Error", 'code' => 401, 'Message' =>  'Invalid request due to headers'];
            }
        } else{            
            return ["Ack" => "Error", 'code' => 401, 'Message' =>  'Invalid request due to headers, access token is not available'];
        }        
    }
}
